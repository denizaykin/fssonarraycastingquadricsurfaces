%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
    limitsOb1 = [min(CP(:,1))-dR, max(CP(:,1)) + dR, min(CP(:,2))-dR, max(CP(:,2)) + dR, min(CP(:,3))-dR, max(CP(:,3)) + dR];
    lb1 = limitsOb1([1, 3, 5]); ub1 = limitsOb1([2 4 6]);
    
    figure(1)
    
    plotOb(Qelp1_W, lb1, ub1);
    hold on
    axis_2 = plotLimits(Limits);
    axis equal
    plot3(CP(:,1),CP(:,2),CP(:,3),'.k', 'Linewidth',1)
    xlabel('X [m]', 'fontSize', 16)
    ylabel('Y [m]', 'fontSize', 16)
    zlabel('Z [m]', 'fontSize', 16)
    axis_3 = axis_2; axis_3(3) = 0;
    axis(axis_3);
    plotDidson;
    view([0.2,-0.3,0.2]);
    
    figure(2)    
    plotOb(Qelp1_W, lb1, ub1);
    hold on
    plot3(CP(:,1),CP(:,2),CP(:,3),'.')
    xlabel('X [m]')
    ylabel('Y [m]')
    zlabel('Z [m]')
    axis equal
    
   
    figure(3)
    hold on
    plot3(CP(:,1),CP(:,2),CP(:,3),'.')
    xlabel('X [m]')
    ylabel('Y [m]')
    zlabel('Z [m]')