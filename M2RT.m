%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [R, T] = M2RT(M)
R = M(1:3,1:3);
T = M(1:3,4);