%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function roots = CubicMA(A)
global eps
if size(A,2) ~= 4
    error('Cubic expected but length(A) is not equal to 4');
end
a = A(:,1);
roots = NaN(size(A,1),3);
inds = find(abs(a) <= eps);
if ~isempty(inds)
    display('Not a cubic')
    roots(inds,2:end) = QuadraticMA(A(inds,2:end));
    %return;
end
indsRest = find(abs(a) > eps);
a = A(indsRest, 1);
b = A(indsRest, 2);
c = A(indsRest, 3);
d = A(indsRest, 4);

i = sqrt(-1);
Delta0 = b.^2 - 3*a.*c;
Delta1 = 2*b.^3 - 9*a.*b.*c + 27*a.^2.*d;
C = power((Delta1+sqrt(Delta1.^2 - 4*Delta0.^3))/2, 1/3);
u2 = (-1+i*sqrt(3))/2; u3 = (-1 - i*sqrt(3))/2; %u1 = 1;

x1 = -1/3./a.*(b + C + Delta0./C);
x2 = -1/3./a.*(b + u2*C + Delta0./C/u2);
x3 = -1/3./a.*(b + u3*C + Delta0./C/u3);

%roots(abs(a) > eps,:) = [x1 x2 x3];
% inds = abs(a) > eps;
% X = [x1 x2 x3];
% roots(inds,:)  = X(inds,:);
X = [x1 x2 x3];
roots(indsRest,:) = X; 