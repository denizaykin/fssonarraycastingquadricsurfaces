%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function roots = QuadraticMA(A)
global eps
if size(A,2) ~= 3
    error('Quadratic expected but length(A) is not equal to 3');
end
a = A(:,1);
roots = NaN(size(A,1),2);
inds = find(abs(a) <= eps);
if ~isempty(inds)
    display('Not a quadratic')
    roots(inds,2) = -A(inds,3)./A(inds,2);
    %return;
end
indsRest = find(abs(a) > eps);
a = A(indsRest,1);
b = A(indsRest,2);
c = A(indsRest,3);

Delta = b.^2 - 4*a.*c;
x1 = (-b + sqrt(Delta))/2./a;
x2 = (-b - sqrt(Delta))/2./a;
%roots(abs(a) > eps,:) = [x1 x2];
% inds = abs(a) > eps;
% X = [x1 x2];
% roots(inds,:)  = X(inds,:);
X = [x1 x2];
roots(indsRest,:) = X; 