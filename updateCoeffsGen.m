%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [coefUpdated, Q_]= updateCoeffsGen(varargin)
if length(varargin) == 2
    co = varargin{1};
    M = varargin{2};
end
if length(varargin) == 3
    co = varargin{1};
    R = varargin{2};
    T = varargin{3};
    M = RT2M(R,T);
end
%invM = inv(M);
coGen = coeffs2coeffsGen(co);
Q = coeffsGen2Mat(coGen);
Q_ = M'*Q*M;
%Q_ = invM*Q*invM';
coGen_ = Mat2coeffsGen(Q_);
coefUpdated = coeffsGen2coeffs(coGen_);