%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function CPW = S2W(CP, Ms)
for j = 1:size(CP,2)
    M = Ms{j}; %for motion j
    for i = 1:size(CP,1)
        if ~isempty(CP{i,j})
            CPW{i,j} = [CP{i,j}, ones(size(CP{i,j},1),1)]*M';
        end
    end
end