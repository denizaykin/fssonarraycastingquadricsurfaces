%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [CP, index] = sortCP2(CP)
if ~iscell(CP)
    index = [];
    if ~isempty(CP)
        %First group with respect to Ti values and then sort each column across
        %increasing z
        x = CP(:,1);
        y = CP(:,2);
        z = CP(:,3);
        Ti = atan(x./y);
        Ri = sqrt(x.^2 + y.^2 + z.^2);
        [Ti, inds] = sort(Ti);
        Ri = Ri(inds);
        x = x(inds);
        y = y(inds);
        z = z(inds);
        index = groupT(Ti); %index are the indices for which T(index(i):index(i+1)-1) are about the same (by eps)
        
        %We perform a rotation trick to be able to sort the patches as a
        %function of increasing z values. Note that if the camera y-axis is
        %aligned with the center of the object then the patches on the
        %object can be obtained as a function of increasing/decreasing z.
        [Rmin, indrmin] = min(Ri);
        zmin = z(indrmin);
        phiMid = mean(asin(zmin./Rmin));
        w = [phiMid, 0 0];
        Rot = rodrigues(w);
        zrot = [x y z]*Rot(:,3);
        %But this did not work..!?
        inds = [];
        for i = 1:length(index)-1
            %[~, indi] = sort(z(index(i):index(i+1)-1)); indi = index(i)-1 + indi;
            [val, indi] = sort(zrot(index(i):index(i+1)-1)); indi = index(i)-1 + indi;
            inds = [inds; indi];
        end
        x = x(inds);
        y = y(inds);
        z = z(inds);
        CP = [x y z];
    end
else
    for i = 1:size(CP,1) %for all objects
        for j = 1:size(CP,2) %for all sonar views
            [CP{i,j}, index{i,j}] = sortCP(CP{i,j});
        end
    end
end