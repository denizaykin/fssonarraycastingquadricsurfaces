%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function Ixy = bb2xyI(I, data, a)
for j = 1:length(I)
    data.frame = I{j}; data.frame(:,1:2) = 255; data.frame(:,end-1:end) = 255; data.frame(1:4,:) = 255; data.frame(end-3:end,:) = 255;
    data = make_new_image(data,data.frame,a); 
    Ixy{j} = data.image;
end