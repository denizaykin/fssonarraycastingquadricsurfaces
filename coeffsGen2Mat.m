%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function Q = coeffsGen2Mat(c)
if size(c,1) > size(c,2)
    c = c';
end
Q = [c(1:4); c([2, 5:7]); c([3, 6, 8:9]); c([4, 7, 9, 10])];