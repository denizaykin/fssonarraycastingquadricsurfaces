%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function Q = getQpl(varargin)
if isempty(varargin)
    Q = [zeros(3) [0 0 1/2]'; 0 0 1/2 1];
else
    n = varargin{1};
    if size(n,1) == 1
        n = n';
    end
    Q = [zeros(3) n/2; n'/2 1];
end