%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function plotOb(c_Q, lb, ub)
global eps3
if ~isempty(lb) & ~isempty(ub)
    if size(c_Q,1) == 1 | size(c_Q,1) == 10
        c = c_Q;
    else
        Q = c_Q;
        c = mat2coeffs(Q);
    end
    
    dx = 1/20*min(ub - lb);
    inds = 0;
    if dx < eps3
        [diff, inds] = sort(ub-lb);
        dx = 1/20*diff(2);
        if dx < eps3
            return
        end
    end
    %gx = lb(1)-dx:dx:ub(1)+dx;
    %gy = lb(2)-dx:dx:ub(2)+dx;
    %gz = lb(3)-dx:dx:ub(3)+dx;
    % dx = 1/20*min(ub - lb);
    
    dd = 0.001;
    if ~inds(1)
        gx = linspace(lb(1)-dd, ub(1) + dd, ceil((ub(1) - lb(1)+ 2*dd)/dx));
        gy = linspace(lb(2)-dd, ub(2) + dd, ceil((ub(2) - lb(2)+ 2*dd)/dx));
        gz = linspace(lb(3)-dd, ub(3) + dd, ceil((ub(3) - lb(3)+ 2*dd)/dx));
        
        % gv = linspace(-1,2,100); % adjust for appropriate domain
        
        [xx, yy, zz]=meshgrid(gx, gy, gz);
        F = c(1)*xx.*xx + c(2)*yy.*yy + c(3)*zz.*zz+ c(4)*xx.*yy + c(5)*xx.*zz + c(6)*yy.*zz + c(7)*xx + c(8)*yy + c(9)*zz + c(10);
        
        %isosurface(xx, yy, zz, F, -eps3)
        %isosurface(xx, yy, zz, F, 0)
        
        %[faces,verts,colors] = isosurface(xx,yy,zz,F,0, zeros(size(zz)));
        [faces,verts] = isosurface(xx,yy,zz,F,0);
        % patch('Vertices', verts, 'Faces', faces, ...
        %     'FaceVertexCData', colors, ...
        %     'FaceColor','interp', ...
        %     'edgecolor', 'interp','facecolor', 'flat', 'edgecolor', 'none');
        patch('Vertices', verts, 'Faces', faces, ...
            'FaceColor', 'g', 'edgecolor', 'none')
        % patch('Vertices', verts, 'Faces', faces, ...
        %     'FaceVertexCData', 'g', ...
        %     'FaceColor','interp', ...
        %     'edgecolor', 'interp','facecolor', 'flat', 'edgecolor', 'none');
    else
        if inds(1) == 1
            gy = linspace(lb(2)-dd, ub(2) + dd, ceil((ub(2) - lb(2)+ 2*dd)/dx));
            gz = linspace(lb(3)-dd, ub(3) + dd, ceil((ub(3) - lb(3)+ 2*dd)/dx));
            [yy, zz] = meshgrid(gy, gz);
            xx = lb(1)*ones(size(yy));
        elseif inds(1) == 2
            gx = linspace(lb(1)-dd, ub(1) + dd, ceil((ub(1) - lb(1)+ 2*dd)/dx));
            gz = linspace(lb(3)-dd, ub(3) + dd, ceil((ub(3) - lb(3)+ 2*dd)/dx));
            [xx, zz] = meshgrid(gx, gz);
            yy = lb(2)*ones(size(xx));
        else
            gx = linspace(lb(1)-dd, ub(1) + dd, ceil((ub(1) - lb(1)+ 2*dd)/dx));
            gy = linspace(lb(2)-dd, ub(2) + dd, ceil((ub(2) - lb(2)+ 2*dd)/dx));
            [xx, yy] = meshgrid(gx, gy);
            zz = lb(3)*ones(size(xx));
        end
       % [xx, yy, zz] = elimOutBnd(xx,yy,zz, lb, ub);
        surf(xx, yy, zz, 0*ones(size(xx)), 'edgecolor', 'none')
    end    
    
end