%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function plotIm(Im)
for j = 1:length(Im)
    subplot(1, length(Im), j)
    imshow(uint8(Im{j})); colormap gray; axis off
end