function [factor a]= GetDistortionCoefs(caseno)

factor=1;

if isempty(caseno)
%%%%%%%  this is it
    a =[0.0044   -0.0056    3.5881   49.0095]; %%%1 & 2& 3 & 4
    display('default distortion coefficients.');
elseif caseno=='1'
    a =[0.0039   -0.0061    3.4654   49.0165]; %%%1 & 2& 3
    display('case no is 1');
elseif caseno=='2'
    a =[0.0044   -0.0060    3.6041   49.0169]; %%%1 & 2
    display('case no is 2');
elseif caseno=='3'
    a =[0.0045    0.0046    3.5867   49.0170]; %%%1 & 2& 3 & 4 & 5
    display('case no is 3');
elseif caseno=='4'
    a =[0.0060    0.0102    3.9419   49.0001]; %%%3 & 4 & 5
    display('case no is 4');
elseif caseno=='5'
    a =[0.0062    0.0095    3.9737   49.0051]; %%%1 & 3 & 4 & 5
    display('case no is 5');
elseif caseno=='6'
    a =[0.0037   -0.0051    2.9175   48.9893];
    a= [0.0038   -0.0068    2.9568   48.5403];
    display('FAU sonar');    
elseif caseno=='7'
    a= [0.0040   -0.0069    3.0100   48.6052];
    display('FAU sonar after one iteration on coefficients');
elseif caseno=='o'                  %%%%%%% Didson setting
    display('didson parameters');
    factor=1.012;
    a=factor*[.0030, -0.0055, 2.6829, 48.04];
elseif caseno=='8'                  %%%%%%% Didson setting
    display('didson parameters');
    %factor = 33698;
    a = [0.            0043956          -0.004407       3.5859       49.332];
    %a = factor * a;
elseif caseno=='9'                  %%%%%%% Didson setting  Murat 04/11/11
    display('didson parameters');
    %factor = 0.9000;
    a = [0.0033    0.0043    3.0758   49.8778];
 

end

return