%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
close all; clear
InitParams2
InitObjects %Initialize the objects
InitSonarMotions2 %Initialize the sonar motions

%return

if isempty(LIMAll)
    CP = callGetCP2(QAll, RR, TT, Limits);
else
    CP = callGetCP4(QAll, LIMAll, RR, TT, Limits);
end

display('Done computing CP')

if isempty(CP)
    warning('No critical points are detected. There is no objects in the field of view.')
    return
end

CPW = S2W(CP, Ms);
if ~isempty(CP)
    figure(1)
    hold on
    camlight; lighting gouraud
    [LB, UB] = getObLimits(CPW);
    if isempty(LIMW)
        plotObs(QQW, LB, UB);
    else
        plotObs2(QQW, CPW, LB, UB, LIMW);
    end
    plotDidsons(Ms);
    plotLimitsLite(Limits, Ms);
    %    showResults
end

if isempty(LIMAll)
    [CPref, CPplot] = callRefineCP(CP, QAll);
else
    CPref = callRefineCPV2(CP, QAll, LIMAll);
end
[CPref, index] = sortCP(CPref);
CP_rtp = cellXYZ2RTP(CPref);
CPW = S2W(CPref, Ms);
if ~isempty(CPW)
    %CPplotW = S2W(CPplot, Ms);
    plotCPs(CPW, index);
    %lighting phong
    xlabel('X [m]', 'fontSize', 16)
    ylabel('Y [m]', 'fontSize', 16)
    zlabel('Z [m]', 'fontSize', 16)
    axis equal  %axis([-0.65 0.65 0 2.5 -0.32 0.32])
    view([0.2,-0.3,0.2]);
else
    warning('No Critical points are found. Object is probably outside the field of view.')
end



%%%%%%%%%%%%%%%%%%%%%%%%%%  FORM IMAGES

if fast %only boundary image
    [Inr, Inr_sq, Inrdp] = ImformDidsonv2(CP_rtp, index, QAll, rmin, rmax, thetaMax, phiMax); 
else
    [Inr, Inr_sq, Inrdp] = ImformDidson(CP_rtp, index, QAll, rmin, rmax, thetaMax, phiMax);
end
%!!!!!!!!Check why Imnrdp returns values smaller than 0
Imnr = I2Im(Inr);
Imnr_sq = I2Im(Inr_sq);
Imnrdp = I2Im(Inrdp); 


%getRealData
load('Realdata/SphereStill1.mat', 'bbavg', 'imavg'); 
bbavg1 = bbavg; imavg1 = imavg;
load('Realdata/SphereStill2.mat', 'bbavg', 'imavg'); 
bbavg2 = bbavg; imavg2 = imavg;
figure(2)
subplot(121)
%imshow(imavg1)
imshow(flipud(bbavg1))
subplot(122)
%imshow(imavg2)
imshow(flipud(bbavg2))


if size(TT,1) == 96
    TT = TT'; RR = RR';
end

Iavg1 = Im2I(bbavg1);
Iavg2 = Im2I(bbavg2);
if length(Inr) == 2
Inr_{1} = Inr{1}*max(Iavg1(:))/max(Inr{1}(:)); Inr_{2} = Inr{2}*max(Iavg2(:))/max(Inr{2}(:));
Inr_sq_{1} = Inr_sq{1}*max(Iavg1(:))/max(Inr_sq{1}(:)); Inr_sq_{2} = Inr_sq{2}*max(Iavg2(:))/max(Inr_sq{2}(:));
Inrdp_{1} = Inrdp{1}*max(Iavg1(:))/max(Inrdp{1}(:)); Inrdp_{2} = Inrdp{2}*max(Iavg2(:))/max(Inrdp{2}(:));

for j = 1:length(Inr)
        figure
        subplot(1, 3, 1)
        imagesc(flipud(Imnr{j})); colormap gray; axis off
        subplot(1, 3, 2)
        imagesc(flipud(Imnr_sq{j})); colormap gray; axis off
        subplot(1, 3, 3)
        imagesc(flipud(Imnrdp{j})); colormap gray; axis off
end
% for i = 1:size(CP_rtp,1)
%     for j = 1:size(CP_rtp,2)
%         noMiss = numberMissed(CP_rtp{i,j}, index{i,j})
%     end
% end

%plot3Int(Imnr, Imnr_sq, Imnrdp, bbavg1, bbavg2)
    plot3Int(RR, TT, Inr_, Inr_sq_, Inrdp_, Iavg1, Iavg2)
end

data = get_frame_first(FileName);
data = make_first_image(data,fact,512, a);
Imnrxy = bb2xyI(Imnr, data, a);
Imnr_sqxy = bb2xyI(Imnr_sq, data, a);
Imnrdpxy = bb2xyI(Imnrdp, data, a);
figure
plotIm(Imnrxy)
figure
plotIm(Imnr_sqxy)
figure
plotIm(Imnrdpxy)
    

figure(11)
hold on
plot(Imnr_sq{1}(:,30:50),'r')
plot(Imnr{1}(:,30:50),'b')
plot(Imnrdp{1}(:,30:50),'g')
plot(bbavg1(:,30:50), 'k')
title('')

I_avg1_ = Iavg1(1:100,30:50); maxI_ = max(I_avg1_(:)); I_avg1_ = double(I_avg1_)/maxI_;
I_sq_ = Inr_sq{1}(1:100,30:50); maxI_sq_ = max(I_sq_(:)); I_sq_ = I_sq_/maxI_sq_;
I_nr_ = Inr{1}(1:100,30:50); maxI_nr_ = max(I_nr_(:)); I_nr_ = I_nr_/maxI_nr_;
I_nrdp_ = Inrdp{1}(1:100,30:50); maxI_nrdp_ = max(I_nrdp_(:)); I_nrdp_ = I_nrdp_/maxI_nrdp_;


figure(12)
hold on
plot(I_avg1_, 'k', 'Linewidth', 2)
plot(I_nr_,'b')
plot(I_sq_,'r')
plot(I_nrdp_,'g')
title('Comparing shading models with Didson signal')
