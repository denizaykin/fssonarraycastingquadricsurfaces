%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function X = getCPw(coef, R, T)
if size(R,2) > 1
    R = R';
end
if length(T) == 1
    T = T*ones(size(R));
end
A = coef(1); B = coef(2); C = coef(3); D = coef(4); E = coef(5);
F = coef(6); G = coef(7); H = coef(8); I = coef(9); J = coef(10);

tanT = tan(T);
tanTsq = tanT.^2;
K = A + B*tanTsq - C*(1 + tanTsq) + D*tanT;
L = G + H*tanT;
M = C*R + J;
N = E + F*tanT;

a = K.^2 + N.^2.*(1+tanTsq);
b = 2*K.*L + 2*N.*I.*(1 + tanTsq);
c = 2*K.*M + L.^2 - (R.*N).^2 + I.^2.*(1+tanTsq);
d = 2*L.*M - 2*N.*I.*R.^2;
e = M.^2 - (I.*R).^2;

A_coef = [a b c d e];
x = QuarticMA(A_coef);
RR = repmat(R',4,1); RR = RR(:);
TT = repmat(T',4,1); TT = TT(:);
x = x(:);
indsImag = find(abs(imag(x)) >= 1e-9);
x(indsImag) = [];
if isempty(x)
    X = [];
    return
end
RR(indsImag) = [];
TT(indsImag) = [];
x = real(x);
tanTT = tan(TT);
y = x.*tanTT;
z = sqrt(RR.^2 - x.^2.*(1+tanTT.^2));

indsBad = find(imag(z) ~= 0);
x(indsBad) = [];
if isempty(x)
    X = [];
    return
end   
y(indsBad) = [];
z(indsBad) = [];


[z, inds] = sort(z);
x = x(inds);
y = y(inds);

Ti = atan(x./y);
indsBad = find(Ti > max(T) | Ti < min(T));
x(indsBad) = [];
if isempty(x)
    X = [];
    return
end  
y(indsBad) = [];
z(indsBad) = [];
X = [x y z];