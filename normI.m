%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function Ipow = normI(I,BN)
NUM = length(I)/BN;
I = reshape(I,NUM,BN);
Ipow = real(1/NUM*sum(I));
Ipow = Ipow/max(Ipow);