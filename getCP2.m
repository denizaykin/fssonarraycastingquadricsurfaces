%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function X = getCP2(co_Q, R, T, Limits)
global eps2
%coef: coefficients defining a quartic
%R: range
%T: theta
%X: critical point on quartic defined by coef
%eps = 1e-7;

if size(co_Q,1) == 1 | size(co_Q,1) == 10
    coef = co_Q;
else
    Q = co_Q;
    coef = mat2coeffs(Q);
end


SIZ = size(R);
if length(T) == 1
    T = T*ones(size(R));
end
if SIZ(1) > 1 & SIZ(2) > 1
    R = R(:);
    T = T(:);
elseif SIZ(2) > 1
    R = R';
    T = T';
end

A = coef(1); B = coef(2); C = coef(3); D = coef(4); E = coef(5);
F = coef(6); G = coef(7); H = coef(8); I = coef(9); J = coef(10);

tanT = tan(T);
tanTsq = tanT.^2;
K = A*tanTsq + B - C*(1 + tanTsq) + D*tanT;
L = G*tanT + H;
M = C*R.^2 + J;
N = E*tanT + F;

a = K.^2 + N.^2.*(1+tanTsq);
b = 2*K.*L + 2*N.*I.*(1 + tanTsq);
c = 2*K.*M + L.^2 - (R.*N).^2 + I.^2.*(1+tanTsq);
d = 2*L.*M - 2*N.*I.*R.^2;
e = M.^2 - (I.*R).^2;

A_coef = [a b c d e];
y = QuarticMA(A_coef);
%  y = NaN(size(A_coef,1),4);
%  for i = 1:size(A_coef,1)
%      ri = roots(A_coef(i,:));
%      y(i,5-length(ri):end) = ri;
%  end

%CHECK and ELIMINATE IF
% 1) isNaN
% 2) imaginary solutions of x
% 3)convert the remaining to real and compute corresponding y, zpos
% 4) eliminate imaginary z solutions
% 5) accept the real solutions and compute zneg = -zpos and add these to
% the solutions set
% 6) eliminate solutions that are outside theta and range min max values
% 7) eliminate solutions that are not on the original quartic
% 8) eliminate duplicate solutions.

%%% 1)
RR = repmat(R',4,1); RR = RR(:);
TT = repmat(T',4,1); TT = TT(:);
y = y'; y = y(:);
indsNaN = isnan(y);
y(indsNaN) = [];
if isempty(y)
    X = []; %RR = []; TT = [];
    return
end
RR(indsNaN) = [];
TT(indsNaN) = [];

%%% 2) 
%Eliminate imaginary or negative y solutions

indsImag = find(abs(imag(y)) >= eps2 | real(y) < 0);
y(indsImag) = [];
if isempty(y)
    X = [];% RR = []; TT = [];
    return
end
RR(indsImag) = [];
TT(indsImag) = [];
%Convert the rest to real and compute the corresponding x and z (positive
%solution).
y = real(y);
tanTT = tan(TT);
x = y.*tanTT;
zpos = sqrt(RR.^2 - y.^2.*(1+tanTT.^2));

%eliminate imaginary z solutions
indsBad = find(abs(imag(zpos)) >= eps2);
x(indsBad) = [];
if isempty(x)
    X = [];% RR = []; TT = [];
    return
end
y(indsBad) = [];
zpos(indsBad) = [];
z = real(zpos);
%TT(indsBad) = [];
%RR(indsBad) = [];

zneg = -zpos;
x = [x; x];
y = [y; y];
z = [zneg; zpos];

%TT = [TT; TT];
%RR = [RR; RR];

% Ti = atan(x./y);
% [Ti, inds] = sort(Ti);
% x = x(inds); 
% y = y(inds);
% z = z(inds);
%Index = groupT(Ti,eps);
 Ri = sqrt(x.^2 + y.^2 + z.^2);
% [Ri, inds] = sort(Ri);
% x = x(inds);
% y = y(inds);
% z = z(inds);
%TT = TT(inds);



%Check if the solution satifies theta and range boundaries
thetaMin = - Limits.thetaMax; thetaMax = Limits.thetaMax;
phiMin = - Limits.phiMax; phiMax = Limits.phiMax;
rmin = Limits.rmin; rmax = Limits.rmax;
Ti = atan(x./y);
Phi = asin(z./Ri);
%indsBad = find(Ti > max(T) + eps2 | Ti < min(T) - eps2 | Ri < min(R) - eps2 | Ri > max(R)+ eps2); %eliminate if theta's are not right.
indsBad = find(Ti > thetaMax + eps2 | Ti < thetaMin - eps2 | Ri < rmin - eps2 | Ri > rmax + eps2 | Phi < phiMin - eps2 | Phi > phiMax + eps2); %eliminate if outside the boundaries
x(indsBad) = [];
if isempty(x)
    X = [];%  RR = []; TT = [];
    return
end
y(indsBad) = [];
z(indsBad) = [];
%TT(indsBad) = [];
%RR(indsBad) = [];
%Check also if the solution satisfies phi boundaries:


%Finally eliminate the points that do not satisfy the original quartic
%equation. Also eliminate duplicate solutions.
X = [x y z];

%Eliminate Duplicate solutions
X = elimDuplicates(X);