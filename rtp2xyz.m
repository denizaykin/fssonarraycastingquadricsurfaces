%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function Po = rtp2xyz(Pi)
Po = [Pi(:,1).*sin(Pi(:,2)).*cos(Pi(:,3)), Pi(:,1).*cos(Pi(:,2)).*cos(Pi(:,3)), Pi(:,1).*sin(Pi(:,3))];
