%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function axis_l = plotLimits2(Limits,M)
global col colno
%[Rot, Tran] = M2RT(M);
rmin = Limits.rmin;
rmax = Limits.rmax;
thetaMax = Limits.thetaMax;
phiMax = Limits.phiMax;
res = 97;
Thetas = linspace(-thetaMax, thetaMax, res)';
Phis = linspace(-phiMax, phiMax, res)';
left_cl = [rmin*ones(res,1), -thetaMax*ones(res,1), Phis];
low_cl = [rmin*ones(res,1), Thetas, -phiMax*ones(res,1)];
right_cl = [rmin*ones(res,1), thetaMax*ones(res,1), Phis];
top_cl = [rmin*ones(res,1), Thetas, phiMax*ones(res,1)];

left_far = [rmax*ones(res,1), -thetaMax*ones(res,1), Phis];
low_far = [rmax*ones(res,1), Thetas, -phiMax*ones(res,1)];
right_far = [rmax*ones(res,1), thetaMax*ones(res,1), Phis];
top_far = [rmax*ones(res,1), Thetas, phiMax*ones(res,1)];

LeC = rtp2xyz(left_cl); LeC = [LeC ones(size(LeC,1),1)]; LeC = LeC*M';%LeC = LeC*Rot + repmat(Tran',size(LeC,1),1);
LoC = rtp2xyz(low_cl);  LoC = [LoC ones(size(LoC,1),1)]; LoC = LoC*M';%LoC = LoC*Rot + repmat(Tran',size(LeC,1),1);
RiC = rtp2xyz(right_cl); RiC = [RiC ones(size(RiC,1),1)]; RiC = RiC*M';%RiC = RiC*Rot + repmat(Tran',size(LeC,1),1);
ToC = rtp2xyz(top_cl);   ToC = [ToC ones(size(ToC,1),1)]; ToC = ToC*M';%ToC = ToC*Rot + repmat(Tran',size(LeC,1),1);

LeF = rtp2xyz(left_far); LeF = [LeF ones(size(LeF,1),1)]; LeF = LeF*M';%LeF = LeF*Rot + repmat(Tran',size(LeC,1),1);
LoF = rtp2xyz(low_far);  LoF = [LoF ones(size(LoF,1),1)]; LoF = LoF*M';%LoF = LoF*Rot + repmat(Tran',size(LeC,1),1);
RiF = rtp2xyz(right_far); RiF = [RiF ones(size(RiF,1),1)]; RiF = RiF*M';%RiF = RiF*Rot + repmat(Tran',size(LeC,1),1);
ToF = rtp2xyz(top_far);   ToF = [ToF ones(size(ToF,1),1)]; ToF = ToF*M';%ToF = ToF*Rot + repmat(Tran',size(LeC,1),1);

plot3(LeC(:,1), LeC(:,2), LeC(:,3), col(colno));
plot3(LoC(:,1), LoC(:,2), LoC(:,3), col(colno));
plot3(RiC(:,1), RiC(:,2), RiC(:,3), col(colno));
plot3(ToC(:,1), ToC(:,2), ToC(:,3), col(colno));
plot3(LeF(:,1), LeF(:,2), LeF(:,3), col(colno));
plot3(LoF(:,1), LoF(:,2), LoF(:,3), col(colno));
plot3(RiF(:,1), RiF(:,2), RiF(:,3), col(colno));
plot3(ToF(:,1), ToF(:,2), ToF(:,3), col(colno));

plot3([LeC(1,1), LeF(1,1)], [LeC(1,2), LeF(1,2)], [LeC(1,3), LeF(1,3)], col(colno))
plot3([LeC(end,1), LeF(end,1)], [LeC(end,2), LeF(end,2)], [LeC(end,3), LeF(end,3)], col(colno))
plot3([RiC(1,1), RiF(1,1)], [RiC(1,2), RiF(1,2)], [RiC(1,3), RiF(1,3)], col(colno))
plot3([RiC(end,1), RiF(end,1)], [RiC(end,2), RiF(end,2)], [RiC(end,3), RiF(end,3)], col(colno))

% Left = [LeC; flipud(LeF)];
% Right = [RiC; flipud(RiF)];
% Bottom = [LoC; flipud(LoF)];
% Top = [ToC; flipud(ToF)];
% Front = [LoC; RiC; flipud(ToC); flipud(LeC)];
% Back = [LoF; RiF; flipud(ToF); flipud(LeF)];

% fill3(Bottom(:,1), Bottom(:,2), Bottom(:,3), 'y');
% for i = 1:res-1
%         patch = [rmax*ones(2*res,1), [Thetas(i)*ones(res,1); Thetas(i+1)*ones(res,1)], [Phis; flipud(Phis)]];
%         patch_xyz = rtp2xyz(patch);
%         fill3(patch_xyz(:,1), patch_xyz(:,2), patch_xyz(:,3), 'y');
% end
% fill3(Left(:,1), Left(:,2), Left(:,3), 'y');

axis_l = [min(LeF(:,1)), max(RiF(:,1)), min(LoC(:,2)), max(LoF(:,2)), min(LoF(:,3)), max(ToF(:,3))];
