%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
% Define small values to account for inaccurate precision
global eps eps2 eps3 epsT epst_R epsUnfit epsimag dxDid dyDid dzDid col colno NUM BN BeN tpl c testcnt caseno fast
fast = 0;
epsimag = 1e-5;
epsUnfit = 1e-2; %this was 1e-2 for the 3 ellipse case
epst_R = 1e-4;
epsT = 0.005;
eps = 1e-4;
eps2 = 1e-4;
eps3 = 1e-4;
dxDid = 0.03;
dyDid = 0.08;
dzDid = 0.015;
col = 'krbcmy';
colno = 1;
NUM = 100;
BN = 512;
BeN = 96;
tpl = 4.4e-6;
c = 1500; % [m/s] : speed of sound in water
testcnt = 1;
%Read calibration parameters for the sonar
caseno='';
    
%Initialize sonar related parameters
rmin = 1.25; windowlength = 1.25; rmax = rmin + windowlength; dR = windowlength/511;
thetaMax = 14.4*pi/180; phiMax = 7*pi/180;

bins_Crit = 0.5:BN; %R_Crit = rmin + (bins_Crit - 1)*dR; %theta = 0;
beam_Crit = 0.5:BeN;
%theta_Crit = pi/180*linspace(-14,14,BeN);
%theta_Crit = theta_Crit(48:49);
%theta_Crit = 0;
%[TT, RR] = meshgrid(theta_Crit, R_Crit);

%[RR, TT] = meshgrid(R_Crit, theta_Crit);
[BB, BeBe] = meshgrid(bins_Crit, beam_Crit);
[RR, TT] = BB2Rtheta(BeBe(:), BB(:), rmin, rmax, caseno, thetaMax*180/pi);
RR = reshape(RR, BeN, BN);
TT = reshape(TT, BeN, BN)*pi/180;

if rmin == 1.25 & rmax == 2.5
    range_ratio = 1;
    FileName = 'vid1_rmin125wl125.ddf';
%     rmin = range_ratio*rmin;
%     rmax = range_ratio*rmax;
end

%Read calibration parameters for the sonar
caseno='';
fact = 4;
[f, cal_a]= GetDistortionCoefs(caseno);
a = fact*cal_a;

%Limits to be scanned
Limits.rmin = rmin;
Limits.rmax = rmax;
Limits.phiMax = phiMax;
Limits.thetaMax = thetaMax;

%Define the sonar motion (i.e. rotation and translation along the
%trajectory)
Rots{1} = eye(3);
Trans{1} = zeros(3,1);

%w2 = [0 -1.4 0]; %causing the problem at the 15th beam with second
%ellipse.
w2 = [0 -pi/2 0];
%Rots{2} = rodrigues(w2);
%Trans{2} = [0 0.006 0.03]'; %sphere for view2 taken on april 11
%Trans{2} = [0 0 0]'; %sphere for view 2 taken on april 15
% Trans{2} = [0 -0.15 0]'; %cone 
% Trans{2} = [0 -0.23 0]'; %cylinder
% Trans{2} = [0.1 -0.01 0]'; %cube

% w3 = [0 0 pi/2];
% Rots{3} = rodrigues(w3);
% Trans{3} = [1.7 1.7 0]';