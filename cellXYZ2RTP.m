%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function RTP = cellXYZ2RTP(XYZ)
if ~iscell(XYZ)
    RTP = [];
    if~isempty(XYZ)
        R = sqrt(XYZ(:,1).^2 + XYZ(:,2).^2 + XYZ(:,3).^2);
        T = atan(XYZ(:,1)./XYZ(:,2));
        P = asin(XYZ(:,3)./R);
        RTP = [R T P];
        return
    end
end
if iscell(XYZ)
    for i = 1:size(XYZ,1)
        for j = 1:size(XYZ,2)
            RTP{i,j} = cellXYZ2RTP(XYZ{i,j});
        end
    end
end