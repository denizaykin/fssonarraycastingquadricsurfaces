%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function plotCoord(O, X, Y, Z)
plot3arr(O,X,'k');
plot3arr(O,Y,'r');
plot3arr(O,Z,'g');