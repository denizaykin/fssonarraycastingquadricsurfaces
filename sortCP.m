%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [CP, index] = sortCP(CP)
if ~iscell(CP)
    index = [];
    if ~isempty(CP)
        %First group with respect to Ti values and then sort each column across
        %increasing z
        x = CP(:,1);
        y = CP(:,2);
        z = CP(:,3);
        Ti = atan(x./y);
        [Ti, inds] = sort(Ti);
        x = x(inds);
        y = y(inds);
        z = z(inds);
        index = groupT(Ti); %index are the indices for which T(index(i):index(i+1)-1) are about the same (by eps)
        %
        inds = [];
        for i = 1:length(index)-1
            %[~, indi] = sort(z(index(i):index(i+1)-1)); indi = index(i)-1 + indi;
            [val, indi] = sort(z(index(i):index(i+1)-1)); indi = index(i)-1 + indi;
            inds = [inds; indi];
        end
        x = x(inds);
        y = y(inds);
        z = z(inds);
        CP = [x y z];
    end
else
    for i = 1:size(CP,1) %for all objects
        for j = 1:size(CP,2) %for all sonar views
            [CP{i,j}, index{i,j}] = sortCP(CP{i,j});
        end
    end
end