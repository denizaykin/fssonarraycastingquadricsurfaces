%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function plotDidson2(M)
global dxDid
global dyDid
global dzDid
dx = dxDid;
dy = dyDid;
dz = dzDid;
C = [0.5 0.5 0.5];


X1 = [-dx, -dy, -dz, 1]'; X1 = M*X1;
X2 = [-dx, -dy, dz, 1]';  X2 = M*X2;
X3 = [dx, -dy, dz, 1]';    X3 = M*X3;
X4 = [dx, -dy, -dz, 1]';   X4 = M*X4;
X5 = [dx, 0, -dz, 1]';    X5 = M*X5;
X6 = [dx, 0, dz, 1]';     X6 = M*X6;
X7 = [-dx, 0, dz, 1]';   X7 = M*X7;
X8 = [-dx, 0, -dz, 1]';  X8 = M*X8;

front = [X5, X6, X7, X8];
back = [X1, X2, X3, X4];
top = [X2, X3, X6, X7];
bottom = [X1, X4, X5, X8];
left = [X1, X2, X7, X8];
right = [X3, X4, X5, X6];


fill3Rec(front, C);
fill3Rec(back, C);
fill3Rec(top, C);
fill3Rec(bottom, C);
fill3Rec(left, C);
fill3Rec(right, C);

Orig = (X2+X6)/2;
plotCoord(Orig, Orig + (X3-X2)/2, Orig + (X6-X3)/2, Orig + (X2-X1)/2);