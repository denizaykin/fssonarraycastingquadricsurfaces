%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [LB, UB] = getObLimits(CP)
%CP : Critical points N x M cell matrix such that N is the number of
%sonar motions, M is the number of objects
if iscell(CP)
    for i = 1:size(CP,1) %For all objects
        CPall_forMotion_j = [];
        for j = 1:size(CP,2) %for all motions
            CPall_forMotion_j = [CPall_forMotion_j; CP{i,j}];
        end
        if ~isempty(CPall_forMotion_j)
            [LB{i}, UB{i}] = getObLimits_core(CPall_forMotion_j);
        else
            LB{i} = [];
            UB{i} = [];
        end
    end
else
    [LB, UB] = getObLimits_core(CP);
end