%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [CP, CPplot] = callRefineCP(CP, Qall)
for j = 1:size(CP,2) %for all motions (i.e. all sonar positions)
    %Refine the points determined at a given sonar position.
    [CP(:,j), CPplot(:,j)] = refineCP(CP(:,j), Qall(:,j));
end