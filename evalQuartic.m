%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function Res = evalQuartic(c_Q, X)
if size(c_Q,1) == 1 | size(c_Q,1) == 10
    coef = c_Q;
    A = coef(1); B = coef(2); C = coef(3); D = coef(4); E = coef(5);
    F = coef(6); G = coef(7); H = coef(8); I = coef(9); J = coef(10);
    Res = A*X(:,1).^2 + B*X(:,2).^2 + C*X(:,3).^2 + ...
      D*X(:,1).*X(:,2) + E*X(:,1).*X(:,3) + F*X(:,2).*X(:,3) + ...
      G*X(:,1) + H*X(:,2) + I*X(:,3) + J;
else
    Q = c_Q;
    X = [X ones(size(X,1),1)];
    QX = X*Q;
    Res = sum(X.*QX,2);
end