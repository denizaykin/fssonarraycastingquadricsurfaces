%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function indsOut = outOfBnds(X, LB, UB, eps)
indsOutLB = zeros(size(X,1),1);
if ~isempty(LB)
    if iscell(LB)
        for k = 1:length(LB)
            if ~isempty(LB{k})
                Reslb = evalQuartic(LB{k}, X);
                indsOutLB = indsOutLB | Reslb < -eps;
            end
        end
    else
        Reslb = evalQuartic(LB, X);
        indsOutLB = indsOutLB | Reslb < -eps;
    end
end
indsOutUB = zeros(size(X,1),1);
if ~isempty(UB)
    if iscell(UB)
        for k = 1:length(UB)
            if ~isempty(UB{k})
                Resub = evalQuartic(UB{k}, X);
                indsOutUB = indsOutUB |Resub > eps;
            end
        end
    else
        Resub = evalQuartic(UB, X);
        indsOutUB = indsOutUB |Resub > eps;
    end
end
indsOut = indsOutLB | indsOutUB;