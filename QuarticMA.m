%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function roots = QuarticMA(A)
global eps
if size(A,2) ~= 5
    error('Quartic expected but length(A) is not equal to 5');
end
a = A(:,1);
roots = NaN(size(A,1),4);
inds = find(abs(a) <= eps);
if ~isempty(inds)
    display('Not a quartic')
    roots(inds,2:end) = CubicMA(A(inds,2:end));
    %return
end
indsRest = find(abs(a) > eps);
a = A(indsRest,1);
b = A(indsRest,2);
c = A(indsRest,3);
d = A(indsRest,4);
e = A(indsRest,5);

Delta0 = c.^2 - 3*b.*d + 12*a.*e;
Delta1 = 2*c.^3 - 9*b.*c.*d + 27*b.^2.*e + 27*a.*d.^2 - 72*a.*c.*e;
Q = power((Delta1 + sqrt(Delta1.^2 - 4*Delta0.^3))/2, 1/3);
p = (8*a.*c - 3*b.^2)./(8*a.^2);
q = (b.^3 - 4*a.*b.*c + 8*a.^2.*d)./(8*a.^3);
S = 1/2*sqrt(-2/3*p + 1/3./a.*(Q + Delta0./Q));

m = -b./a/4;
n = 1/2*sqrt(-4*S.^2 - 2*p + q./S);
o = 1/2*sqrt(-4*S.^2 - 2*p - q./S);

x1 = m - S + n;
x2 = m - S - n;
x3 = m + S + o;
x4 = m + S - o;
% inds = abs(a) > eps;
% X = [x1 x2 x3 x4];
% roots(inds,:)  = X(inds,:);
X = [x1 x2 x3 x4];
roots(indsRest,:) = X; 