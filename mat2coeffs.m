%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function coeffs = mat2coeffs(K)
if size(K,1) == 3
    coeffs = [K(1,1) K(2,2) K(3,3) 2*K(1,2) 2*K(1,3) 2*K(2,3) 0 0 0 -1];
else
    coeffs = [K(1,1) K(2,2) K(3,3) 2*K(1,2) 2*K(1,3) 2*K(2,3) 2*K(1,4) 2*K(2,4) 2*K(3,4) K(4,4)];
end