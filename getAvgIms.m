%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a)
data = get_frame_first(FileName);
data = make_first_image(data,fact,512, a);
bbavg = double(data.frame);
for cnt = 2:fmax
    data = get_frame_new(data,cnt);
    data = make_new_image(data,data.frame,a);
    bbavg = bbavg + double(data.frame);
end
bbavg = uint8(bbavg/fmax);
data = make_new_image(data,data.frame,a);
imavg = data.image;