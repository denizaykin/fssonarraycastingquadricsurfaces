%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [Obsph, ObLim] = getQsph2(varargin)
if length(varargin) == 0
    rad = 1;
    hlb = [];
    hub = [];
    solid = 1;
elseif length(varargin) == 1
    rad = varargin{1};
    hlb = [];
    hub = [];
    solid = 1;
elseif length(varargin) == 2
    rad = varargin{1};
    hlb = varargin{2};
    hub = [];
    solid = 1;
elseif length(varargin) == 3
    rad = varargin{1};
    hlb = varargin{2};
    hub = varargin{3};
    solid = 1;
else
    rad = varargin{1};
    hlb = varargin{2};
    hub = varargin{3};
    solid = varargin{4};
end
    
Qsp = [eye(3) [0 0 0]'; 0 0 0 -1];
M = getMag(rad*ones(3,1));
Qspm = inv(M)*Qsp*inv(M)';

%Get boundaries
Qlb = getQplv2();
Qub = Qlb;
if ~isempty(hlb)
Tlb = [0 0 -hlb];
%wub = [0 0 0]';
Rlb = eye(3);
Qlb = tr2W(Qlb, Rlb, Tlb);
else
Qlb = [];
end

if ~isempty(hub)
Tub = [0 0 -hub]';
Rub = eye(3);
Qub = tr2W(Qub, Rub, Tub);
else
    Qub = [];
end

Obsph{1} = Qspm;
ObLim.LB{1}{1} = Qlb; %Lower limit for Qsph
ObLim.UB{1}{1} = Qub; %High limit for Qsph

if solid
    if ~isempty(Qlb)
        Obsph{2} = Qlb;
        ObLim.LB{2}{1} = [];
        ObLim.UB{2}{1} = Qspm; %High limit for Qlb
        if ~isempty(Qub)
            Obsph{3} = Qub;
            ObLim.LB{3}{1} = [];
            ObLim.UB{3}{1} = Qspm; %High limit for Qub
        end
    elseif ~isempty(Qub)
            Obsph{2} = Qub;
            ObLim.LB{2}{1} = [];
            ObLim.UB{2}{1} = Qspm; %High limit for Qub
    end
end
    