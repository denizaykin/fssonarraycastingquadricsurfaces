%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function plot3Int(RR, TT, Imnr, Imnr_sq, Imnrdp, bbavg1, bbavg2)


tAll = TT(:); rAll = RR(:);
xAll = rAll.*sin(tAll);
yAll = rAll.*cos(tAll);
for j = 1:length(Imnr)
        zAllnr = Imnr{j}(:);
        zAllnr_sq = Imnr_sq{j}(:);
        zAllnrdp = Imnrdp{j}(:);
        indsNZ = zAllnrdp > 0;
        figure(j + length(Imnr) + 2)
        hold on
        plot3(xAll(indsNZ), yAll(indsNZ), zAllnr(indsNZ), '.b');
        plot3(xAll(indsNZ), yAll(indsNZ), zAllnr_sq(indsNZ), '.r');
        plot3(xAll(indsNZ), yAll(indsNZ), zAllnrdp(indsNZ), '.g');
        xlabel('xs', 'fontSize', 16)
        ylabel('ys', 'fontSize', 16)
        zlabel('Image pixel Intensity', 'fontSize', 16)
end
    figure(length(Imnr) + 3)
    zAllReal1 = bbavg1(:);
    plot3(xAll(indsNZ), yAll(indsNZ), zAllReal1(indsNZ), '.k');
    %surf(xAll(indsNZ), yAll(indsNZ), zAllReal1(indsNZ))
    figure(length(Imnr) + 4)
    zAllReal2 = bbavg2(:);
    plot3(xAll(indsNZ), yAll(indsNZ), zAllReal2(indsNZ), '.k');
    %surf(xAll(indsNZ), yAll(indsNZ), zAllReal(indsNZ))