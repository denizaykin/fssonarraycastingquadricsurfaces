%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
%cone
r=linspace(0.1,0.2,25);
theta = linspace(0,2*pi,25);
[r,theta] = meshgrid(r,theta);
x = r.*cos(theta);
y = r.*sin(theta);
z = fliplr(r)-0.1;
figure(2)
%mesh(x,y,z)
%C = 100*ones(size(z));
surf(x,y,z)
axis equal

%disk
r = linspace(0,0.1,25);
theta = linspace(0,2*pi,25);
[r,theta] = meshgrid(r,theta);
x = r.*cos(theta);
y = r.*sin(theta);
z = 0.1*ones(size(y));
figure(2)
hold on
surf(x,y,z, 0.35*ones(size(x)))
colormap hsv


% %disk (for cone top)
% r=0.1;
% dtheta=0.01;
% theta=linspace(0,2*pi,100)';
% n=numel(theta);
% [x,y]=pol2cart(theta,r);
% %figure;
% cmap=colormap(jet);
% X=[zeros(1,n-1);x(1:end-1)';x(2:end)';zeros(1,n-1)];
% Y=[zeros(1,n-1);y(1:end-1)';y(2:end)';zeros(1,n-1)];
% %Zcone=[zeros(1,n-1);-ones(1,n-1);-ones(1,n-1);zeros(1,n-1)];
% Z = -ones(4,n-1);
% %C=numel(cmap)*(1:n-1)/n-1;
% C = 200*ones(1,n-1);
% patch(X,Y,Z,C,'EdgeAlpha',0);
% axis equal;
% colormap gray