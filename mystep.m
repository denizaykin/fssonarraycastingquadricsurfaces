%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function out = mystep(t)
out = zeros(size(t));
out(t>=0) = 1;
