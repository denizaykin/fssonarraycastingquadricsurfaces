%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [lb, ub] = getObLimits_core(CP)
dx = 0.0;
limitsOb = [min(CP(:,1))-dx, max(CP(:,1)) + dx, min(CP(:,2))-dx, max(CP(:,2)) + dx, min(CP(:,3))-dx, max(CP(:,3)) + dx];
lb = limitsOb([1, 3, 5]); ub = limitsOb([2 4 6]);