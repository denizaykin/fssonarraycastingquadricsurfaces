%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function rect = myrect(t, ti, tpl)
rect = [mystep(t-ti) - mystep(t-(ti+tpl))];