%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function I = Im2I(Im)
if ~iscell(Im)
    I = (10.^(90/4800*double(Im))-1)*1e-12;
else
    for i = 1:size(I,1)
        for j = 1:size(I,2)
            I{i,j} = Im2I(Im{i,j});
        end
    end
end
