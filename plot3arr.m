%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function plot3arr(x1, x2, col)
quiver3(x1(1),x1(2), x1(3), x2(1)-x1(1), x2(2) - x1(2), x2(3)- x1(3), col);
