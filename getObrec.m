%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [Obrec, ObLim] = getObrec(varargin)
%l: length
%w: width
%h: height
if isempty(varargin)
    l = 1;
    w = 1;
    h = 1;
    solid = 1;
elseif length(varargin) == 3
    l = varargin{1};
    w = varargin{2};
    h = varargin{3};
    solid = 1;
elseif length(varargin) == 4
    l = varargin{1};
    w = varargin{2};
    h = varargin{3};
    solid = varargin{4};
end
%b: bottom, t: top
%l: left,   r: right
%f: front,  ba: back


%Get objects
Qb = getQplv2();
Qt = Qb;
Tt = [0 0 -h]';
Rt = eye(3);
Qt = tr2W(Qt, Rt, Tt);

Qlr = getQplv2([1 0 0]);
Tl  = [l/2 0 0]';
Tr  = [-l/2 0 0]';
Ql  = tr2W(Qlr, Rt, Tl);
Qr  = tr2W(Qlr, Rt, Tr);

Qfba = getQplv2([0 1 0]);
Tf   = [0 w/2 0]';
Tba  = [0 -w/2 0]';
Qf = tr2W(Qfba, Rt, Tf);
Qba = tr2W(Qfba, Rt, Tba);

%Get boundaries
Qb_lb{1} = Qf;
Qb_lb{2} = Ql;
Qb_ub{1} = Qba;
Qb_ub{2} = Qr;

Qt_lb{1} = Qf;
Qt_lb{2} = Ql;
Qt_ub{1} = Qba;
Qt_ub{2} = Qr;

Qf_lb{1} = Qb;
Qf_lb{2} = Ql;
Qf_ub{1} = Qt;
Qf_ub{2} = Qr;

Qba_lb{1} = Qb;
Qba_lb{2} = Ql;
Qba_ub{1} = Qt;
Qba_ub{2} = Qr;

Ql_lb{1} = Qf;
Ql_lb{2} = Qb;
Ql_ub{1} = Qba;
Ql_ub{2} = Qt;

Qr_lb{1} = Qf;
Qr_lb{2} = Qb;
Qr_ub{1} = Qba;
Qr_ub{2} = Qt;


Obrec{1} = Ql;
Obrec{2} = Qr;
Obrec{3} = Qf;
Obrec{4} = Qba;



ObLim.LB{1}{1} = Ql_lb{1}; 
ObLim.LB{1}{2} = Ql_lb{2};
ObLim.UB{1}{1} = Ql_ub{1}; 
ObLim.UB{1}{2} = Ql_ub{2};

ObLim.LB{2}{1} = Qr_lb{1}; 
ObLim.LB{2}{2} = Qr_lb{2};
ObLim.UB{2}{1} = Qr_ub{1}; 
ObLim.UB{2}{2} = Qr_ub{2};

ObLim.LB{3}{1} = Qf_lb{1}; 
ObLim.LB{3}{2} = Qf_lb{2};
ObLim.UB{3}{1} = Qf_ub{1}; 
ObLim.UB{3}{2} = Qf_ub{2};

ObLim.LB{4}{1} = Qba_lb{1}; 
ObLim.LB{4}{2} = Qba_lb{2};
ObLim.UB{4}{1} = Qba_ub{1}; 
ObLim.UB{4}{2} = Qba_ub{2};

if solid
    Obrec{5} = Qb;
    Obrec{6} = Qt;
    ObLim.LB{5}{1} = Qb_lb{1}; %Lower limit for bottom
    ObLim.LB{5}{2} = Qb_lb{2};
    ObLim.UB{5}{1} = Qb_ub{1}; %High limit for bottom
    ObLim.UB{5}{2} = Qb_ub{2};
    
    ObLim.LB{6}{1} = Qt_lb{1}; %Lower limit for top
    ObLim.LB{6}{2} = Qt_lb{2};
    ObLim.UB{6}{1} = Qt_ub{1}; %High limit for top
    ObLim.UB{6}{2} = Qt_ub{2};
end