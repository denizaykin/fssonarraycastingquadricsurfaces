%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [CP, CPplot] = elimShPtsv2(CP, Q, LB, UB)
global epsimag epst_R 
eps_ = 0.05;
R = sqrt(CP(:,1).^2 + CP(:,2).^2 + CP(:,3).^2);
T = atan(CP(:,1)./CP(:,2));
P = atan(CP(:,3)./R);
X2Y2 = sqrt(CP(:,1).^2 + CP(:,2).^2);
sinT = CP(:,1)./X2Y2;
cosT = CP(:,2)./X2Y2;
sinP = CP(:,3)./R;
cosP = X2Y2./R;
u = [CP./repmat(R,1,3) zeros(size(CP,1),1)];
p = [zeros(size(CP)), ones(size(CP,1),1)];
pQ = p*Q';
uQ = u*Q';
uQp = -sum(u.*pQ,2); %N by 4
uQu = sum(u.*uQ,2);
pQp = sum(p.*pQ,2);
delta = sqrt(uQp.^2 - uQu.*pQp);
%t1 = (uQp + delta)./uQu;

diff = uQp - delta;
t2 = diff./uQu;
indsZero = (uQu == 0);
t2(indsZero) = pQp(indsZero)./(2*uQp(indsZero));
indsZeroZero = (uQp == 0) & indsZero;
t2(indsZeroZero) = R(indsZeroZero);

indsNoCross = abs(imag(t2)) > epsimag;
t2(indsNoCross) = R(indsNoCross);
t2 = real(t2);

X = [t2.*sinT.*cosP t2.*cosT.*cosP t2.*sinP];
indsOut = outOfBnds(X, LB, UB, 0);
t2(indsOut) = R(indsOut);
% if ~isempty(LB)
%     Reslb = evalQuartic(LB, X);
%     indsOutLB = Reslb < 0;
%     t2(indsOutLB) = R(indsOutLB);
% end
% if ~isempty(UB)
%     Resub = evalQuartic(UB, X);
%     indsOutUB = Resub > 0;
%     t2(indsOutUB) = R(indsOutUB);
% end


%Note that we need R <= t2 then t2 - R >= 0 or t2 - R >= -eps2
%So we need to eliminate t2-R < -eps2
indsBad = real(t2-R) < -epst_R;
CPplot = repmat(t2./R,1,3).*CP;
CPplot(indsBad,:) = [];
CP(indsBad,:) = [];