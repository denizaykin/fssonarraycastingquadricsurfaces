%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function Im = I2Im(I)
if ~iscell(I)
    %I = I/max(I(:))*3.16e-8;
    I = I/max(I(:))*3.16e-8;
    Im = real(240/90*(20*log10(I*1e12+1)));
    Im(Im < 0) = 0;
else
    for i = 1:size(I,1)
        for j = 1:size(I,2)
            Im{i,j} = I2Im(I{i,j});
        end
    end
end
% function Im = I2Im(I, fk)
% if ~iscell(I)
%     I = I/max(I(:))*3.16e-8*fk;
%     Im = real(240/90*(20*log10(I*1e12+1)));
% else
%     for i = 1:size(I,1)
%         for j = 1:size(I,2)
%             Im{i,j} = I2Im(I{i,j}, fk(i));
%         end
%     end
% end