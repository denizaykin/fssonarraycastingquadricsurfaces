%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [K,Q,D,W,l,qf] = ellipsoid(varargin)
%
%  [K,Q,D,W,l,qf] = ellipsoid(...)
%
%  Draws the ellipsoid 
%      x' * K * x = 1
%    corresponding to the positive definite symmetric 3 by 3 matrix K 
%
%  ellipsoid  with no arguments sets K = eye(3) - a sphere
%  ellipsoid(A)    sets K = A if A is symmetric; otherwise K = A' * A
%  ellipsoid(A,n)  same as ellipsoid(A) with n the number of mesh points
%                    default is n = 25
%  ellipsoid(A,t)  same as ellipsoid(A) with translation vector t
%  ellipsoid(A,t,n) same as ellipsoid(A,t) with n the number of mesh points
%  ellipsoid(a,b,c)  sets K = diag([a b c])
%  ellipsoid(a,b,c,n)  sets K = diag([a b c]) and n = # mesh points
%  
%
%  K - symmetric positive definite matrix used
%  Q - columns are eigenvectors or principal axes of ellipse
%  D - diagonal matrix with eigenvalues, so K = Q' * D * Q
%  l = sqrt(diag(D)) - lengths of axes of ellipse
%  W = Q*D^(-1/2) - linear transformation mapping unit sphere to ellipsoid
%  qf - string giving quadratic form x'*K*x
%
%  See also ELLIPSE, TR3
%

switch length(varargin)
  case 0
     A = eye(3);  n = 25; t=zeros(3,1);
  case 1 
     A = varargin{1}; n = 25; t = zeros(3,1);
  case 2 
     A = varargin{1}; n = varargin{2}; t = zeros(3,1);
     if(length(n) > 1) 
        n = 25;
        t = varargin{2};
     end
  case 3 
      if(length(varargin{2}) == 1)
          t = zeros(3,1);
          A = diag([varargin{1},varargin{2},varargin{3}]); n = 25;
      else
          A = varargin{1};
          t = varargin{2};
          n = varargin{3};
      end
  case 4 
     A = diag([varargin{1},varargin{2},varargin{3}]); n = varargin{4};
end

if A == A' 
   K = A;
   else
   K = A' * A;
end

if K(1,1) == 1, q1s = ''; else q1s = [num2str(K(1,1)),' ']; end
if K(2,2) == 1, q2s = ''; else q2s = [num2str(K(2,2)),' ']; end
if K(3,3) == 1, q3s = ''; else q3s = [num2str(K(3,3)),' ']; end

if t(1)~=0 & t(2) ~= 0 & t(3) ~= 0
    p12s = [' (x-',num2str(t(1)), ')(y-',num2str(t(2)), ')'];
    p13s = [' (x-',num2str(t(1)), ')(z-',num2str(t(3)), ')'];
    p23s = [' (y-',num2str(t(2)), ')(z-',num2str(t(3)), ')'];
elseif t(1) ~= 0 & t(2) ~= 0
    p12s = [' (x-',num2str(t(1)), ')(y-',num2str(t(2)), ')'];
    p13s = [' (x-',num2str(t(1)), ')z'];
    p23s = [' (y-',num2str(t(2)), ')z'];
elseif t(1) ~= 0 & t(3) ~= 0
    p12s = [' (x-',num2str(t(1)), ')y'];
    p13s = [' (x-',num2str(t(1)), ')(z-',num2str(t(3)), ')'];
    p23s = [' y(z-',num2str(t(3)), ')'];
elseif t(2) ~= 0 & t(3) ~= 0
    p12s = [' x(y-',num2str(t(2)), ')'];
    p13s = [' x(z-',num2str(t(3)), ')'];
    p23s = [' (y-',num2str(t(2)), ')(z-',num2str(t(3)), ')'];
elseif t(1) ~= 0
    p12s = [' (x-',num2str(t(1)), ')y'];
    p13s = [' (x-',num2str(t(1)), ')z'];
    p23s = [' y z'];
elseif t(2) ~= 0
    p12s = [' x(y-',num2str(t(2)), ')'];
    p13s = [' x z'];
    p23s = [' (y-',num2str(t(2)), ')z'];
elseif t(3) ~= 0
    p12s = [' x y'];
    p13s = [' x(z-',num2str(t(3)), ')'];
    p23s = [' y(z-',num2str(t(3)), ')'];
else
    p12s = [' x y'];
    p13s = [' x z'];
    p23s = [' y z'];
end


q12 = abs(2*K(1,2)); q12x = num2str(q12);
s12 = sign(K(1,2));
if s12 > 0, q12s = [' + ',q12x, p12s]; 
else if s12 < 0, q12s = [' - ',q12x, p12s];
else q12s = '';
end 
end 
q13 = abs(2*K(1,3)); q13x = num2str(q13);
s13 = sign(K(1,3));
if s13 > 0, q13s = [' + ',q13x, p13s]; 
else if s13 < 0, q13s = [' - ',q13x, p13s];
else q13s = '';
end 
end 
q23 = abs(2*K(2,3)); q23x = num2str(q23);
s23 = sign(K(2,3));
if s23 > 0, q23s = [' + ',q23x, p23s]; 
else if s23 < 0, q23s = [' - ',q23x, p23s];
else q23s = '';
end 
end 
t1s = num2str(t(1));
t2s = num2str(t(2));
t3s = num2str(t(3));


if t(1)~=0 & t(2) ~= 0 & t(3) ~= 0
    qf = [q1s,'(x-',t1s,')^2',q12s,' + ',q2s,'(y-', t2s, ')^2',q13s,q23s,' + ',q3s,'(z-', t3s, ')^2'];
elseif t(1) ~= 0 & t(2) ~= 0
    qf = [q1s,'(x-',t1s,')^2',q12s,' + ',q2s,'(y-', t2s, ')^2',q13s,q23s,' + ',q3s,'z^2'];
elseif t(1) ~= 0 & t(3) ~= 0
    qf = [q1s,'(x-',t1s,')^2',q12s,' + ',q2s,'y^2',q13s,q23s,' + ',q3s,'(z-', t3s, ')^2'];
elseif t(2) ~= 0 & t(3) ~= 0
    qf = [q1s,'x^2',q12s,' + ',q2s,'(y-', t2s, ')^2',q13s,q23s,' + ',q3s,'(z-', t3s, ')^2'];
elseif t(1) ~= 0
    qf = [q1s,'(x-',t1s,')^2',q12s,' + ',q2s,'y^2',q13s,q23s,' + ',q3s,'z^2'];
elseif t(2) ~= 0
    qf = [q1s,'x^2',q12s,' + ',q2s,'(y-', t2s, ')^2',q13s,q23s,' + ',q3s,'z^2'];
elseif t(3) ~= 0
    qf = [q1s,'x^2',q12s,' + ',q2s,'y^2',q13s,q23s,' + ',q3s,'(z-', t3s, ')^2'];
else
    qf = [q1s,'x^2',q12s,' + ',q2s,'y^2',q13s,q23s,' + ',q3s,'z^2'];
end

disp(' ')
disp('Quadratic form')
disp(' ')
disp(qf)

disp(' ')

disp('K = ')
disp(K)

[Q,D] = eig(K);

disp('Eigenvectors - principal axes')
disp(' ')
disp(Q)
disp('Eigenvalues')
ev = diag(D);
disp(' ')
disp(ev)
disp('Lengths of principal axes')
l = 1./sqrt(ev);
disp(' ')
disp(l)

W = Q * 1/sqrt(D);

df = linspace(0,pi,n); 
dt = [0:2*pi/n:2*pi]; 

x0 = sin(df)' * cos(dt); 
y0 = sin(df)' * sin(dt);
z0 = cos(df)' * ones(size(dt));

[x,y,z] = tr3(W,x0,y0,z0);
x = x + t(1); y = y + t(2); z = z + t(3);

surf(x,y,z);

W1 = W(1,:); W2 = W(2,:); W3 = W(3,:); 
N = diag([1 + 1/norm(W(:,1)),1 + 1/norm(W(:,2)),1 + 1/norm(W(:,3))]);
WN = W * N;
WN1 = WN(1,:); WN2 = WN(2,:);  WN3 = WN(3,:);

alpha = 0.4;
xa = alpha*[W1;WN1]; xa_ = xa + t(1);
ya = alpha*[W2;WN2]; ya_ = ya + t(2);
za = alpha*[W3;WN3]; za_ = za + t(3);
line(xa_,ya_,za_);

xb = - xa; xb_ = xb + t(1);
yb = - ya; yb_ = yb + t(2);
zb = - za; zb_ = zb + t(3);
line(xb_,yb_,zb_);

axis equal;
shading interp;
colormap(bone);

title(['Ellipsoid:   ',qf,' - 1 = 0']);
xlabel('X [m]')
ylabel('Y [m]')
zlabel('Z [m]')
%axis([min(0,min(xb_(:))), max(xa_(:)), min(0, min(yb_(:))), max(ya_(:)), min(0,min(zb_(:))), max(za_(:))])