%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [Qcon, LB, UB] = getQconv2(varargin)
if isempty(varargin)
    rad = 1; 
    h = 1;
elseif length(varargin) == 1
    rad = varargin{1};
    h = 1;
else
    rad = varargin{1};
    h = varargin{2};
end
Qcon = [[eye(3) ; [0 0 0]'; 0 0 0 0];
M = getMag([rad, rad, 1]);
Qcon = inv(M)*Qcon*inv(M)';