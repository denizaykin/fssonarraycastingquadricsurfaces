
1
On Matlab prompt run

>> testQuartic2

This performs ray casting for frustum of sphere and plots results published in

Efficient Ray-casting of Quadric Surfaces for Forward-Scan Sonars


2
Uncomment the desired object in InitObjects.m and rerun 

>> testQuartic2

Currently, there are 11 sample objects ready to simulate

 0 One Ellipse
 1 Three Ellipses
 2 Semi - sphere with ground plane
 3 Sphere with Limits
 4 Cylinder along z-axis
 5 Cylinder along x-axis
 6 Cone along z-axis
 7 Cylinder with Limits
 8 Cone with Limits
 9 Rectangular Prism
 10 Rectangular Prism: Cube

3 
To do:
  i.  Generate various images of these targets from different views
  ii. Generate your own objects and images

4 
Once a critical amount of images for different targets and views are obtained, ML models for object recognition may be trained/tested.
Need to also incorporate noise to make ML model noise invariant and hopefully get it to work on real data. 
Training on a combination of sim and real data (i.e. real data being augmented by sim data to be able to train more complex Neural Networks) and validating trained ML model on a real dataset would be an interesting exploration. 