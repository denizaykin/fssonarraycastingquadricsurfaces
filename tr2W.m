%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function Q_ = tr2W(varargin)
%transform a quartic to another coordinate frame
if length(varargin) == 2
    Q = varargin{1};
    M = varargin{2};
end
if length(varargin) == 3
    Q = varargin{1};
    R = varargin{2};
    T = varargin{3};
    M = RT2M(R,T);
end
if isempty(Q)
    Q_ = [];
else
    Q_ = M'*Q*M;
end