%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
%R = sym('R', [3,3]);
%T = sym('T', [3,1]);
syms r11 r12 r13 r22 r23 r33 t1 t2 t3
syms A B C D E F G H I J
%syms A_ B_ C_ D_ E_ F_ G_ H_ I_ J_
syms X Y Z

X_ = r11*X + r12*Y + r13*Z + t1;
Y_ = r12*X + r22*Y + r23*Z + t2;
Z_ = r13*X + r23*Y + r33*Z + t3;

Q1 = A*X_^2 + B*Y_^2 + C*Z_^2 + D*X_*Y_ + E*X_*Z_ + F*Y_*Z_ + G*X_ + H*Y_ + I*Z_ + J;
Q2 = A_*X^2 + B_*Y^2 + C_*Z^2 + D_*X*Y + E_*X*Z + F_*Y*Z + G_*X + H_*Y + I_*Z + J_;
 fA_ = simplify(solve(Q1 == A_*X^2 + B_*Y^2 + C_*Z^2 + D_*X*Y + E_*X*Z + F_*Y*Z + G_*X + H_*Y + I_*Z + J_, A_));
% fB_ = simplify(solve(Q1 == Q2, B_));
% fC_ = simplify(solve(Q1 == Q2, C_));
% fD_ = simplify(solve(Q1 == Q2, D_));
% fE_ = simplify(solve(Q1 == Q2, E_));
% fF_ = simplify(solve(Q1 == Q2, F_));
% fG_ = simplify(solve(Q1 == Q2, G_));
% fH_ = simplify(solve(Q1 == Q2, H_));
% fI_ = simplify(solve(Q1 == Q2, I_));
% fJ_ = simplify(solve(Q1 == Q2, J_));