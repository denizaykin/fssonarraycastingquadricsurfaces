%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function Y = mat2vec(varargin)
X = varargin{1};
if length(varargin) > 1
    if varargin{2} == 2
        X = X';
    end
end

for i = 1:size(X,1) %For all objects
    Y{i} = [];
    for j = 1:size(X,2) %for all motions
        Y{i} = [Y{i}; X{i,j}];  %So Y{i} are the points on the same object for all sonar motions.
        %Note that if varargin{2} == 2 then Y{i} are all critical points for a given sonar position i.
    end
end