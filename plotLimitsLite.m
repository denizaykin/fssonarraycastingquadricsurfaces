%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function plotLimitsLite(varargin)
global col colno
Limits = varargin{1};
if length(varargin) == 2;
    if iscell(varargin{2})
        Ms = varargin{2};
    else
        Ms{1} = varargin{2};
    end
else
    Rots = varargin{2};
    Trans = varargin{3};
    if iscell(Rots)
        for i = 1:length(Rots)
            Ms{i} = RT2M(Rots{i},Trans{i});
        end
    else
        Ms{1} = RT2M(Rots, Trans);
    end
end

M0 = [eye(3), [0 0 0]'; 0 0 0 1]; M0 = M0(:);
for i = 1:length(Ms)
    colno = mod(i-1, length(col)) + 1;
    if sum(Ms{i}(:) == M0) == 16
        plotLimits(Limits);
    else
        plotLimits2(Limits,Ms{i});
    end
end