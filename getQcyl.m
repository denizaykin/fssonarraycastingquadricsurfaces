%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function Qcyl = getQcyl(varargin)
if isempty(varargin)
    rad = 1;
else
    rad = varargin{1};
end
Qcyl = [[eye(2) [0 0]'; [0 0 0]] [0 0 0]'; 0 0 0 -1];
M = getMag(rad*ones(3,1));
Qcyl = inv(M)*Qcyl*inv(M)';