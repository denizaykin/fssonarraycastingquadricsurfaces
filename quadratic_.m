%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function roots = quadratic_(A)
if length(A) ~= 3
    error('Quadratic expected but length(A) is not equal to 3');
end
a = A(1);
if abs(a) <= eps 
    display('Not a quadratic')
    roots = -A(3)/A(2);
    return
end
b = A(2);
c = A(3);

Delta = b^2 - 4*a*c;
x1 = (-b + sqrt(Delta))/2/a;
x2 = (-b - sqrt(Delta))/2/a;
roots = [x1 x2]';