%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function X = elimUnfit(X, coef) %Note: I've replaced the places of (coef, X) to (X, coef) on March 26 so earlier caller functions may return an error.
global epsUnfit
Res = evalQuartic(coef, X);
X(abs(Res) > epsUnfit,:) = [];