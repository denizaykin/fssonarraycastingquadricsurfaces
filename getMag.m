%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function M = getMag(m)
% if size(m,1) == 1
%     m = m';
% end
M = [diag(m), [0 0 0]'; 0 0 0 1];