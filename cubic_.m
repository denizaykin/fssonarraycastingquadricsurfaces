%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function roots = cubic_(A)
if length(A) ~= 4
    error('Cubic expected but length(A) is not equal to 4');
end
a = A(1);
if abs(a) <= eps 
    display('Not a cubic')
    roots = quadratic_(A(2:end));
    return
end
b = A(2);
c = A(3);
d = A(4);

i = sqrt(-1);
Delta0 = b^2 - 3*a*c;
Delta1 = 2*b^3 - 9*a*b*c + 27*a^2*d;
C = power((Delta1+sqrt(Delta1^2 - 4*Delta0^3))/2, 1/3);
u2 = (-1+i*sqrt(3))/2; u3 = (-1 - i*sqrt(3))/2; %u1 = 1;

x1 = -1/3/a*(b + C + Delta0/C);
x2 = -1/3/a*(b + u2*C + Delta0/C/u2);
x3 = -1/3/a*(b + u3*C + Delta0/C/u3);
roots = [x1 x2 x3]';
