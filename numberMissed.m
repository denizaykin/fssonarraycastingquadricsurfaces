%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function noMiss = numberMissed(CP_rtp, index)
noMiss = [];
if ~isempty(CP_rtp)
R = CP_rtp(:,1);
rdiff = R(1:end-1)- R(2:end);
indbad = find(abs(rdiff) > 0.0026);
noMiss = length(indbad) - length(index);
end