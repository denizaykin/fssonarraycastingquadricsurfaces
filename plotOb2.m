%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function plotOb2(c_Q, CP, lb, ub, LB, UB)
global eps3
if ~isempty(lb) & ~isempty(ub)
    if size(c_Q,1) == 1 | size(c_Q,1) == 10
        c = c_Q;
    else
        Q = c_Q;
        c = mat2coeffs(Q);
    end
    
    dx = 1/20*min(ub - lb);
    inds = 0;
    if dx < eps3
        [diff, inds] = sort(ub-lb);
%         dx = 1/20*diff(2);
%         if dx < eps3
%             return
%         end
    end
    %gx = lb(1)-dx:dx:ub(1)+dx;
    %gy = lb(2)-dx:dx:ub(2)+dx;
    %gz = lb(3)-dx:dx:ub(3)+dx;
    % dx = 1/20*min(ub - lb);
    
    dd = 0.001;
 
        gx = linspace(lb(1)-dd, ub(1) + dd, min(ceil((ub(1) - lb(1)+ 2*dd)/dx), 40));
        gy = linspace(lb(2)-dd, ub(2) + dd, min(ceil((ub(2) - lb(2)+ 2*dd)/dx),40));
        gz = linspace(lb(3)-dd, ub(3) + dd, min(ceil((ub(3) - lb(3)+ 2*dd)/dx),40));
        
        % gv = linspace(-1,2,100); % adjust for appropriate domain
        
        [xx, yy, zz]=meshgrid(gx, gy, gz);
        F = c(1)*xx.*xx + c(2)*yy.*yy + c(3)*zz.*zz+ c(4)*xx.*yy + c(5)*xx.*zz + c(6)*yy.*zz + c(7)*xx + c(8)*yy + c(9)*zz + c(10);
        
        %isosurface(xx, yy, zz, F, -eps3)
        %isosurface(xx, yy, zz, F, 0)
        
        %[faces,verts,colors] = isosurface(xx,yy,zz,F,0, zeros(size(zz)));
        [faces,verts] = isosurface(xx,yy,zz,F, 0);
        % patch('Vertices', verts, 'Faces', faces, ...
        %     'FaceVertexCData', colors, ...
        %     'FaceColor','interp', ...
        %     'edgecolor', 'interp','facecolor', 'flat', 'edgecolor', 'none');
        
        indsvertsOut = outOfBnds(verts, LB, UB, 1e-3);
          indsOut = find(indsvertsOut);
          rowelim = [];
        for i = indsOut'
            for j = 1:size(faces,1)
                if sum(faces(j,:) == i) > 0
                    rowelim = [rowelim j];
                end
            end
        end
        rowelim = unique(rowelim);
        faces(rowelim,:) = [];
        
%         tri = delaunay(CP(:,1),CP(:,2),CP(:,3));
%         trisurf(tri,CP(:,1),CP(:,2),CP(:,3), zeros(size(CP(:,1))), 'edgecolor', 'none');
        
        patch('Vertices', verts, 'Faces', faces, ...
            'FaceColor', 'g', 'edgecolor', 'none')
        % patch('Vertices', verts, 'Faces', faces, ...
        %     'FaceVertexCData', 'g', ...
        %     'FaceColor','interp', ...
        %     'edgecolor', 'interp','facecolor', 'flat', 'edgecolor', 'none');
   
    
    
end