%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
%Conical Ellipse equation: A = [a, b, c, d, e, f]'
% g(y,z,A) = ay^2 + byz + cz^2 + dy + ez + f = 0
%or g(y,z,A) = y^2 + byz + cz^2 + dy + ez + f = 0 where A = A/a
% So we have 5 parameters, namely b, c, d, e, f

%We also have the motion equation in between frames 1 and i = 2,3,4.. such
%that 
%P1 = RiPi + Ti
%where P1 is the position of a point in the first frame and Ri, Ti are the
%rotation and translation matrices/vectors (the rotation and translation of
%sonar) in between.
%Here we use wx rotation and ty, tz translation.


syms b c d e f  %the coefficients of ellipse
syms wx ty tz %the motion parameters
syms Ri Z_ %The range and Z_ (at ith frame) position of a point in the i'th frame
%Y_ = sqrt(Ri^2 - Z_^2); %Then the Y_ at ith frame is given by this
%equation since Y_^2 + Z_^2 = Ri
Y = cos(wx)*sqrt(Ri^2-Z_^2) - sin(wx)*Z_ + ty;
Z = sin(wx)*sqrt(Ri^2-Z_^2) - cos(wx)*Z_ + tz;

%Insert into ellipse equation
Ell = Y^2 + b*Y*Z + c*Z^2 + d*Y + e*Z + f
