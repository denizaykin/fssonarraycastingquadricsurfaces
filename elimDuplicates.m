%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function X = elimDuplicates(X)
global eps
for i = 1:3
diff = X(1:end-1,:) - X(2:end,:);
indsDup = sum(abs(diff),2) < eps;
% if (isempty(indsDup))
%     break;
% end
X(indsDup,:) = [];
end
