%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
close all; clear
InitParams
InitObjects %Initialize the objects
InitSonarMotions %Initialize the sonar motions

CP = getCP2(Qelp1_W, RR, TT, Limits);


if ~isempty(CP)
    CP = elimShPts(CP,Qelp1_W); %Has to be improved to account for other objects in the fov. Or write another function that does this!
    %CP = elimshPts2(CP, QQ);
end
if ~isempty(CP)
    CP = elimUnfit(CP, Qelp1_W); %eliminate the points that are not on the quartic.
end
if ~isempty(CP)
%     figure(1)
%     plotLimitsLite(Limits, Rots, Trans);
%     plotObs(QQ, CPs);
%     plotDidsons(Rots, Trans);
%     plotCPs(CPs, Rots, Trans);
showResults
else
    warning('No Critical points are found. Object is probably outside the field of view.')
end


