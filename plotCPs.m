%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function plotCPs(CP, index)
global col colno
if ~iscell(CP) & ~isempty(CP)
    for i = 1:length(index)-1
        %[~, indi] = sort(z(index(i):index(i+1)-1)); indi = index(i)-1 + indi;
        %[val, indi] = sort(z(index(i):index(i+1)-1)); indi = index(i)-1 + indi;
        x = CP(:,1); y = CP(:,2); z = CP(:,3);
        plot3(x(index(i):index(i+1)-1), y(index(i):index(i+1)-1), z(index(i):index(i+1)-1),[col(colno), '.'], 'Linewidth',1);
    end
else
    for i = 1:size(CP,1) %for all objects
        for j = 1:size(CP,2) %for all sonar views
            colno = mod(j-1, length(col)) + 1;
            plotCPs(CP{i,j}, index{i,j});    
        end
    end
end
%
% global col colno
% if ~iscell(CP) & ~isempty(CP)
%     %First group with respect to Ti values and then sort each column across
%     %increasing z
%     x = CP(:,1);
%     y = CP(:,2);
%     z = CP(:,3);
%     Ti = atan(x./y);
%     [Ti, inds] = sort(Ti);
%     x = x(inds);
%     y = y(inds);
%     z = z(inds);
%     index = groupT(Ti); %index are the indices for which T(index(i):index(i+1)-1) are about the same (by eps)
%     %
%     inds = [];
%     for i = 1:length(index)-1
%         [~, indi] = sort(z(index(i):index(i+1)-1)); indi = index(i)-1 + indi;
%         %fnplt(cscvn([x(indi), y(indi), z(indi)]'),col(colno),2);
%         %plot3(x(indi), y(indi), z(indi),'k', 'Linewidth',2);
%         plot3(x(indi), y(indi), z(indi),[col(colno), '.'], 'Linewidth',1);
%         inds = [inds; indi];
%     end
%     x = x(inds);
%     y = y(inds);
%     z = z(inds);
%     CP = [x y z];
% else
%     for i = 1:size(CP,1) %for all objects
%         for j = 1:size(CP,2) %for all sonar views
%             colno = mod(j-1, length(col)) + 1;
%             CP{i,j} = plotCPs(CP{i,j});
%         end
%     end
% end