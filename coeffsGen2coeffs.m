%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function coef = coeffsGen2coeffs(coeffGen)
if size(coeffGen,1) > size(coeffGen,2)
    coeffGen = coeffGen';
end
coef = [coeffGen([1, 5, 8]), 2*coeffGen([2, 3, 6, 4, 7, 9]), coeffGen(10)]';