%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function Qcon = getQcon(varargin)
if isempty(varargin)
    rad = 1; 
elseif length(varargin) == 1
    rad = varargin{1};
else
    rad = varargin{1};
end
Qcon = [eye(3) [0 0 0]'; 0 0 0 0]; Qcon(3,3) = -1;
M = getMag([rad, rad, 1]);
Qcon = inv(M)*Qcon*inv(M)';

