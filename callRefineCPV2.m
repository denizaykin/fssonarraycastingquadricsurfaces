%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function CP = callRefineCPV2(CP, Qall, LIMAll)
for j = 1:size(CP,2) %for all motions (i.e. all sonar positions)
    %Refine the points determined at a given sonar position.
    CP(:,j) = refineCPv2(CP(:,j), Qall(:,j), LIMAll.LB(:,j), LIMAll.UB(:,j));
end