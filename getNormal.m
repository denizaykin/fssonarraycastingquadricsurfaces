%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function N = getNormal(X,Q)
if size(X,2) == 3
X = [X ones(size(X,1),1)];
end
N = X*Q(1:4,1:3);
N = N(:,1:3);
N = N./ repmat(sqrt(N(:,1).^2 + N(:,2).^2 + N(:,3).^2),1,3);