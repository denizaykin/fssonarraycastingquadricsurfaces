%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
%%% VIEW 1
InitParams2
fmax = 100;
%%%Sphere
% %sphere Motion
% FileName = sprintf('C:/Users/Murat/Desktop/Shapes1_DidsonData_041114/view1/2014-04-11_205952_HFsphereMot.ddf'); 
% [bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a);
% save('Realdata/sphereMot1.mat', 'bbavg', 'imavg', 'data')
% 
% %sphere Still
% FileName = sprintf('C:/Users/Murat/Desktop/Shapes1_DidsonData_041114/view1/2014-04-11_210631_HFsphereStill2.ddf');
% [bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a);
% save('Realdata/sphereStill1.mat', 'bbavg', 'imavg', 'data')


%sphere Motion
FileName = sprintf('C:/Users/Murat/Desktop/Shapes2_DidsonData4_15_2014/view1/2014-04-15_113339_HFsphereMot.ddf'); 
[bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a);
save('Realdata/sphereMot1.mat', 'bbavg', 'imavg', 'data')

%sphere Still
FileName = sprintf('C:/Users/Murat/Desktop/Shapes2_DidsonData4_15_2014/view1/2014-04-15_113243_HFsphereStill.ddf');
[bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a);
save('Realdata/sphereStill1.mat', 'bbavg', 'imavg', 'data')

%%%Cone
%cone Motion
FileName = sprintf('C:/Users/Murat/Desktop/Shapes1_DidsonData_041114/view1/2014-04-11_191817_HFconeMot2.ddf');
[bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a);
save('Realdata/coneMot1.mat', 'bbavg', 'imavg', 'data')

%cone Still
FileName = sprintf('C:/Users/Murat/Desktop/Shapes1_DidsonData_041114/view1/2014-04-11_192006_HFconeStill2.ddf');
[bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a);
save('Realdata/coneStill1.mat', 'bbavg', 'imavg', 'data')


%%%Cylinder
%cylinder Motion
FileName = sprintf('C:/Users/Murat/Desktop/Shapes1_DidsonData_041114/view1/2014-04-11_203543_HFcylinderMot.ddf');
[bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a);
save('Realdata/cylinderMot1.mat', 'bbavg', 'imavg', 'data')

%cylinder Still  DON'T HAVE CYLINDER STILL FOR VIEW 1
FileName = sprintf('C:/Users/Murat/Desktop/Shapes1_DidsonData_041114/view1/2014-04-11_203543_HFcylinderMot.ddf');
[bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a);
save('Realdata/cylinderStill1.mat', 'bbavg', 'imavg', 'data')


%%%Cube
%cube Motion
FileName = sprintf('C:/Users/Murat/Desktop/Shapes1_DidsonData_041114/view1/2014-04-11_194819_HFcubeMot.ddf');
[bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a);
save('Realdata/cubeMot1.mat', 'bbavg', 'imavg', 'data')

%cube Still 
FileName = sprintf('C:/Users/Murat/Desktop/Shapes1_DidsonData_041114/view1/2014-04-11_194900_HFcubeStill.ddf');
[bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a);
save('Realdata/cubeStill1.mat', 'bbavg', 'imavg', 'data')




%%% VIEW 2

%%%Sphere
% %sphere Motion
% FileName = sprintf('C:/Users/Murat/Desktop/Shapes1_DidsonData_041114/view2_90Degrees/_2014-04-11_212824_HFsphereMot.ddf'); 
% [bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a);
% save('Realdata/sphereMot2.mat', 'bbavg', 'imavg', 'data')
% 
% %sphere Still
% FileName = sprintf('C:/Users/Murat/Desktop/Shapes1_DidsonData_041114/view2_90Degrees/_2014-04-11_212732_HFsphereStill.ddf');
% [bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a);
% save('Realdata/sphereStill2.mat', 'bbavg', 'imavg', 'data')

%sphere Motion
FileName = sprintf('C:/Users/Murat/Desktop/Shapes2_DidsonData4_15_2014/view2/2014-04-15_110204_HFsphereMot.ddf'); 
[bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a);
save('Realdata/sphereMot2.mat', 'bbavg', 'imavg', 'data')

%sphere Still
FileName = sprintf('C:/Users/Murat/Desktop/Shapes2_DidsonData4_15_2014/view2/2014-04-15_110415_HFsphereStill.ddf');
[bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a);
save('Realdata/sphereStill2.mat', 'bbavg', 'imavg', 'data')

%%%Cone
%cone Motion
FileName = sprintf('C:/Users/Murat/Desktop/Shapes1_DidsonData_041114/view2_90Degrees/_2014-04-11_225938_HFconMot2v2.ddf');
[bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a);
save('Realdata/coneMot2.mat', 'bbavg', 'imavg', 'data')

%cone Still
FileName = sprintf('C:/Users/Murat/Desktop/Shapes1_DidsonData_041114/view2_90Degrees/_2014-04-11_225729_HFconStill2.ddf');
[bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a);
save('Realdata/coneStill2.mat', 'bbavg', 'imavg', 'data')


%%%Cylinder
%cylinder Motion
FileName = sprintf('C:/Users/Murat/Desktop/Shapes1_DidsonData_041114/view2_90Degrees/2014-04-11_221019_HFcylinderMot.ddf');
[bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a);
save('Realdata/cylinderMot2.mat', 'bbavg', 'imavg', 'data')

%cylinder Still  DON'T HAVE CYLINDER STILL FOR VIEW 1
FileName = sprintf('C:/Users/Murat/Desktop/Shapes1_DidsonData_041114/view2_90Degrees/2014-04-11_221056_HFcylinderStill.ddf');
[bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a);
save('Realdata/cylinderStill2.mat', 'bbavg', 'imavg', 'data')


%%%Cube
%cube Motion
FileName = sprintf('C:/Users/Murat/Desktop/Shapes1_DidsonData_041114/view2_90Degrees/2014-04-11_223237_HFcubeMot.ddf');
[bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a);
save('Realdata/cubeMot2.mat', 'bbavg', 'imavg', 'data')

%cube Still 
FileName = sprintf('C:/Users/Murat/Desktop/Shapes1_DidsonData_041114/view2_90Degrees/2014-04-11_223329_HFcubStill.ddf');
[bbavg, imavg, data] = getAvgIms(FileName, fmax, fact, a);
save('Realdata/cubeStill2.mat', 'bbavg', 'imavg', 'data')
