%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [Obcyl, ObLim] = getQcyl2(varargin)
if isempty(varargin)
    rad = 1;
    h = 1;
    solid = 1;
elseif length(varargin) == 1
    rad = varargin{1};
    h = 1;
    solid = 1;
elseif length(varargin) == 2
    rad = varargin{1};
    h = varargin{2};
    solid = 1;
else
    rad = varargin{1};
    h = varargin{2};
    solid = varargin{3};
end
Qcyl = [[eye(2) [0 0]'; [0 0 0]] [0 0 0]'; 0 0 0 -1];
M = getMag(rad*ones(3,1));
Qcyl = inv(M)*Qcyl*inv(M)';

%Get boundaries
Qlb = getQplv2();
Qub = Qlb;
Tub = [0 0 -h]';
wub = [0 0 0]';
Rub = rodrigues(wub);
Qub = tr2W(Qub, Rub, Tub);

Obcyl{1} = Qcyl;
ObLim.LB{1} = Qlb; %Lower limit for Qcyl
ObLim.UB{1} = Qub; %High limit for Qcyl

if solid
    Obcyl{2} = Qlb;
    Obcyl{3} = Qub;
    ObLim.LB{2} = [];
    ObLim.LB{3} = [];
    ObLim.UB{2} = Qcyl; %High limit for Qlb
    ObLim.UB{3} = Qcyl; %High limit for Qub
end