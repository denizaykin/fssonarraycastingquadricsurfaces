%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [Q_, Lim_]  = tr2Wv2(varargin)
%transform a quartic to another coordinate frame
if length(varargin) == 3
    Q = varargin{1};
    Lim = varargin{2};
    M = varargin{3};
end
if length(varargin) == 4
    Q = varargin{1};
    Lim = varargin{2};
    R = varargin{3};
    T = varargin{4};
    M = RT2M(R,T);
end
for i = 1:length(Q)
    Q_{i} = M'*Q{i}*M;
    if ~isempty(Lim.LB{i})
        Lim_.LB{i} = M'*Lim.LB{i}*M;
    else
        Lim_.LB{i} = [];
    end
    if ~isempty(Lim.UB{i})
        Lim_.UB{i} = M'*Lim.UB{i}*M; 
    else
        Lim_.UB{i} = [];
    end
end