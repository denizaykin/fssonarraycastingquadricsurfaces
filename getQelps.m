%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function Qelps = getQelps(varargin)
%input 1: radii of the ellipse in 3-D (i.e. rad is 3 x 1 vector)
%input 2: flag if 1 draw figure and show the ellipse created else if 0 do not draw 
if isempty(varargin)
    rad = 1;
    figflag = 0;
elseif length(varargin) == 1
    rad = varargin{1};
    figflag = 0;
else
    rad = varargin{1};
    figflag = varargin{2};
end
Qsp = [eye(3) [0 0 0]'; 0 0 0 -1];
M = getMag(rad);
Qelps = inv(M)*Qsp*inv(M)';

if figflag
    c = mat2coeffs(Qelps);
    lb = -rad';
    ub = rad';
    plotOb(c, lb, ub)
    axis equal
end