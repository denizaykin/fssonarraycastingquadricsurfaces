%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function plotObs2(QQ, CP, LB, UB, LIM)
if iscell(QQ)
    for i = 1:length(LB)
        plotOb2(QQ{i}, CP{i}, LB{i}, UB{i}, LIM.LB{i}, LIM.UB{i});  %improve plotOb2 by passing through the real boundaries of the i'th object instead of min max of CP's
    end
elseif iscell(LB)
    plotOb(QQ, LB{1}, UB{1});
else
    plotOb(QQ, LB, UB);
end