%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [rng teta] = BB2Rtheta(beam, bin, rmin, rmax, caseno, TetaMax)
%%%%%%%%%%%%%%%!!!!!!!!!!!!!!Returns teta in degrees not radians!!!!!!!!!!
%rng = (bin-1)/ratior + rmin;
rng = (bin-1)*(rmax-rmin)/511 + rmin;  %bin = 1 + (rng-rmin)*511/(rmax-rmin);
[factor cal_a] = GetDistortionCoefs(caseno);

         for kk= 1:length(rng)
            an=[cal_a(1) cal_a(2) cal_a(3) (cal_a(4)-beam(kk))];
            tetas=roots(an);
            cont=0;
            for j=1:3
                if isreal(tetas(j))==1 & abs(tetas(j))<TetaMax
                   cont=cont+1;
                end
            end
            if cont>1
                teta(kk)= min(tetas);            
            elseif cont==0
                teta(kk)= 10^10;
            else
                teta(kk)= tetas(find(imag(tetas)==0));

            end
         end
         teta= teta(:);
         if size(teta,1) ~= size(rng,1)
             rng = rng';
         end

end