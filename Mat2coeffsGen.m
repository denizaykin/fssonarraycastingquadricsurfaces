%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function c = Mat2coeffsGen(Q)
c = Q([1, 2, 3, 4, 6, 7, 8, 11, 12, 16]);