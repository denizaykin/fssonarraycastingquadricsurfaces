%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function coefGen = coeffs2coeffsGen(coef)
if length(coef) ~= 10
    error('10 coefficients are expected')
end
coefGen = zeros(10,1);
coefGen([1, 5, 8, 10]) = coef([1:3, 10]);
coefGen([2 3 4 6 7 9]) = coef([4 5 7 6 8 9])/2;