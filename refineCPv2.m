%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [CP, CPplot] = refineCPv2(CP, Q, LB, UB)
%CP are the critical points at a given sonar position.
%Q is the quartics represented wrt to a given sonar position.
%LIM are the quartic boundaries that limit Q's.
if ~iscell(CP)
     if ~isempty(CP)
         CP = elimUnfit(CP, Q); %eliminate the points that are not on the quartic.
     end
    if ~isempty(CP)
        CP = elimShPtsv2(CP,Q,LB,UB); %Has to be improved to account for other objects in the fov. Or write another function that does this! 
                                        % Improvement DONE!!!
        %CP = elimshPts2(CP, QQ);
    end
    
else
    for i = 1:length(CP) %for all the objects in a given scene
        if ~isempty(CP{i})
            CP{i} = elimUnfit(CP{i}, Q{i});
            for j = 1:length(CP)
                CP{i} = elimShPtsv2(CP{i}, Q{j}, LB{j}, UB{j});
                %         for j = [1:i-1 i+1:length(CP)]
                %             CP{i} = elimShPts(CP{i},Q{j});
                %         end
            end
        end
    end
    
    %May be improved for speed by assigning a CP_all to get rid of the
    %outer for loop.
end