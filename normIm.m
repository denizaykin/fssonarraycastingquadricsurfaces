%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function out = normIm(in, minval, Maxval)
%Written by Murat Aykin
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Normalizes an input (in) (an image in our case)  between the desired ranges  
%(i.e. pixel intensities: minval = 0, Maxval  = 255).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin < 2
    minval = 0;
    Maxval = 255;
elseif nargin < 3
    Maxval = 255;

end
in = double(in);
in = in - min(in(:));
M = max(in(:));
out = in*(Maxval-minval)/M +minval;
out = uint8(out);