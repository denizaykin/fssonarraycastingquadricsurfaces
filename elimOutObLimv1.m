%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function X = elimOutObLimv1(X, Bnd, lbflag)
%global eps2
if lbflag == 0
    UB = Bnd;
    Res = evalQuartic(UB, X);
    X(Res > 0,:) = [];
else
    LB = Bnd;
    Res = evalQuartic(LB, X);
    X(Res < 0,:) = [];
end