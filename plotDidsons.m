%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function plotDidsons(varargin)
if length(varargin) == 1;
    if iscell(varargin{1})
        Ms = varargin{1};
    else
        Ms{1} = varargin{1};
    end
else
    Rots = varargin{1};
    Trans = varargin{2};
    if iscell(Rots)
        for i = 1:length(Rots)
            Ms{i} = RT2M(Rots{i},Trans{i});
        end
    else
        Ms{1} = RT2M(Rots, Trans);
    end
end
for i = 1:length(Ms)
        plotDidson2(Ms{i});
end
