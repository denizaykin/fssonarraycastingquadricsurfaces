%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [Q_, Lim_]  = tr2Wv3(varargin)
%transform a quartic to another coordinate frame
if length(varargin) == 3
    Q = varargin{1};
    Lim = varargin{2};
    M = varargin{3};
end
if length(varargin) == 4
    Q = varargin{1};
    Lim = varargin{2};
    R = varargin{3};
    T = varargin{4};
    M = RT2M(R,T);
end
for i = 1:length(Q)
    Q_{i} = M'*Q{i}*M;
    if ~isempty(Lim.LB{i})
        for k = 1:length(Lim.LB{i})
            if ~isempty(Lim.LB{i}{k})
                Lim_.LB{i}{k} = M'*Lim.LB{i}{k}*M;
            else 
                Lim_.LB{i}{k} = [];
            end
        end
    else
        Lim_.LB{i} = [];
    end
    if ~isempty(Lim.UB{i})
        for k = 1:length(Lim.UB{i})
            if ~isempty(Lim.UB{i}{k})
                Lim_.UB{i}{k} = M'*Lim.UB{i}{k}*M;
            else 
                Lim_.UB{i}{k} = [];
            end
        end
    else
        Lim_.UB{i} = [];
    end
end