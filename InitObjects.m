%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
%Define the quadratic
Qelp1 = getQelps([0.1 0.2 0.3]',0); %Initializing an ellipse
%Transform to world coordinate frame:
% T1 = [0.1 -1.7 -0.3]';
% w1 = [0.2 pi/2 0.2]';
Telp1 = [0 -2 0]';
welp1 = [0 pi/3 0]';
Relp1 = rodrigues(welp1);
%Transform the quartic in world coordinate frame
Qelp1_W = tr2W(Qelp1, Relp1, Telp1);


%Ellipse
Qelp2 = getQelps([0.15 0.15 0.2]',0);
Telp2 = [0 -1.7 0.2]';
welp2 = [0 0 0]';
Relp2 = rodrigues(welp2);
Qelp2_W = tr2W(Qelp2, Relp2, Telp2);


Qelp3 = getQelps([0.05 0.04 0.06]',0);
Telp3 = [0.3 -1.5 0.4]';
welp3 = [0 0 0]';
Relp3 = rodrigues(welp3);
Qelp3_W = tr2W(Qelp3, Relp3, Telp3);

%Sphere
Qsph1 = getQsp(0.14);
Tsph1 = [0 -1.52-0.14 0]';
wsph1 = [0 0 0]';
Rsph1 = rodrigues(welp1);
Qsph1_W = tr2W(Qsph1, Rsph1, Tsph1);

%Sphere with limits
%[Obsph1, LimObsph1] = getQsph2(0.138, [], 0.125);
%Tsph2 = [0.04 -1.525-0.14 0]'; %for the first scene captured on April 11
[Obsph1, LimObsph1] = getQsph2(0.135, [], 0.125);
Tsph2 = [-0.01 -1.28-0.135 0]';
wsph2 = [0 0 0]';
Rsph2 = rodrigues(wsph2);
[Obsph1_W, LimObsph1_W] = tr2Wv3(Obsph1, LimObsph1, Rsph2, Tsph2);

%Plane
% Qpl1 = getQpl([0 0 1]);
% Tpl1 = [0 0 0.35]';
% wpl1 = [-0.8 0 0]';
Qpl1 = getQpl([0 0 1]);
Tpl1 = [0 0 -0.45]';
wpl1 = [-0.3 0 0]';
Rpl1 = rodrigues(wpl1);
Qpl1_W = tr2W(Qpl1, Rpl1, Tpl1);

%Cylinder
Qcyl1 = getQcyl(0.1);
Tcyl1 = [0 -1.8 0]';
wcyl1 = [0 0 0]';
Rcyl1 = rodrigues(wcyl1);
Qcyl1_W = tr2W(Qcyl1, Rcyl1, Tcyl1);

Qcyl2 = getQcyl(0.1); %write a version getQcyl2(rad, height)
Tcyl2 = [0 -1.8 0]';
wcyl2 = [0 pi/2 0]';
Rcyl2 = rodrigues(wcyl2);
Qcyl2_W = tr2W(Qcyl2, Rcyl2, Tcyl2);


%Cylinder with limits
[Obcyl1, LimObcyl1] = getQcyl2(0.1,0.1, 1);
Tcyl3 = [0 -1.59 -0.05]';
wcyl3 = [0 0 0]';
Rcyl3 = rodrigues(wcyl3);
[Obcyl1_W, LimObcyl1_W] = tr2Wv2(Obcyl1, LimObcyl1, Rcyl3, Tcyl3);

%Cone
Qcon1 = getQcon(0.2);
Tcon1 = [0 -1.8 0]';
wcon1 = [0 0 0]';
Rcon1 = rodrigues(wcon1);
Qcon1_W = tr2W(Qcon1, Rcon1, Tcon1);

%Cone with limits
[Obcon1, LimObcon1] = getQcon2(0.1, 0.4, 0.1, 1);
Tcon2 = [0 -1.8 0.15]';
wcon2 = [0 0 0]';
Rcon2 = rodrigues(wcon2);
[Obcon1_W, LimObcon1_W] = tr2Wv2(Obcon1, LimObcon1, Rcon2, Tcon2);

%Reverse con with limits 
[Obcon2, LimObcon2] = getQcon2rev(0.091, 0.17, 0.11, 1);
Tcon3 = [0 -1.57 0]';
wcon3 = [0 0 0]';
Rcon3 = rodrigues(wcon3);
[Obcon2_W, LimObcon2_W] = tr2Wv2(Obcon2, LimObcon2, Rcon3, Tcon3);

%Rectangular Prism
[Obrec1, LimObrec1] = getObrec(0.3, 0.2, 0.1);
Trec1 = [0 -1.8 0.15]';
wrec1 = [0 0 0]';
Rrec1 = rodrigues(wrec1);
[Obrec1_W, LimObrec1_W] = tr2Wv3(Obrec1, LimObrec1, Rrec1, Trec1);

%Rectangular Prism: Cube
[Obcub1, LimObcub1] = getObrec(0.101, 0.101, 0.101);
Tcub1 = [0.08 -1.5875 0.2]';
wcub1 = [-0.1 0 0.04]';
Rcub1 = rodrigues(wcub1);
[Obcub1_W, LimObcub1_W] = tr2Wv3(Obcub1, LimObcub1, Rcub1, Tcub1);

%%%%%%%%%%%%%%%%%%
LIMW = [];
QQW = [];

% % 0 One Ellipse
% QQW{1} = Qelp1_W;

% % %1 Three Ellipses
% QQW{1} = Qelp1_W;
% QQW{2} = Qelp2_W;
% QQW{3} = Qelp3_W;
%fk = [1 1 1]; %Is not accounted for yet.

% %2 Semi - sphere with ground plane.
% QQW{2} = Qpl1_W;
% QQW{1} = Qsph1_W;
% fk = [0.6 1]; %Is not accounted for yet.

% 3 Sphere with Limits
k = length(QQW);
for i = 1:length(Obsph1_W)
    QQW{k+i} = Obsph1_W{i};
    for j = 1:length(LimObsph1_W.LB{i})
        LIMW.LB{k+i}{j} = LimObsph1_W.LB{i}{j};
    end
    for j = 1:length(LimObsph1_W.UB{i})
        LIMW.UB{k+i}{j} = LimObsph1_W.UB{i}{j};
    end
end

%4 Cylinder along z-axis
% QQW{2} = Qpl1_W;
% QQW{1} = Qcyl1_W;

%5 Cylinder along x-axis
% QQW{1} = Qcyl2_W;

%6 Cone along z-axis
% QQW{1} = Qcon1_W;

%7 Cylinder with Limits
% k = length(QQW);
% for i = 1:length(Obcyl1_W)
%     QQW{k+i} = Obcyl1_W{i};
%     LIMW.LB{k+i} = LimObcyl1_W.LB{i};
%     LIMW.UB{k+i} = LimObcyl1_W.UB{i};
% end

%8 Cone with Limits
% k = length(QQW);
% for i = 1:length(Obcon2_W)
%     QQW{k+i} = Obcon2_W{i};
%     LIMW.LB{k+i}{1} = LimObcon2_W.LB{i};
%     LIMW.UB{k+i}{1} = LimObcon2_W.UB{i};
% end


%9 Rectangular Prism
% k = length(QQW);
% for i = 1:length(Obrec1_W)
%     QQW{k+i} = Obrec1_W{i};
%     for j = 1:length(LimObrec1_W.LB{i})
%         LIMW.LB{k+i}{j} = LimObrec1_W.LB{i}{j};
%     end
%     for j = 1:length(LimObrec1_W.UB{i})
%         LIMW.UB{k+i}{j} = LimObrec1_W.UB{i}{j};
%     end
% end

%10 Rectangular Prism: Cube
% k = length(QQW);
% for i = 1:length(Obcub1_W)
%     QQW{k+i} = Obcub1_W{i};
%     for j = 1:length(LimObcub1_W.LB{i})
%         LIMW.LB{k+i}{j} = LimObcub1_W.LB{i}{j};
%     end
%     for j = 1:length(LimObcub1_W.UB{i})
%         LIMW.UB{k+i}{j} = LimObcub1_W.UB{i}{j};
%     end
% end


% QelpTest = [25,0,2.15876699232669e-15,-5.00000000000000; ...
%             0,44.4444444444445,0,-75.5555555555556; ...
%             2.15876699232669e-15,0,44.4444444444445,4.44444444444445; ...
%             -5.00000000000000,-75.5555555555556,4.44444444444445,128.888888888889];
% QelpTest = [25, 0,             0,               -5; ...
%             0,  44.4444444444445,0,               -75.5555555555556; ...
%             0,   0,              44.4444444444445,4.44444444444445; ...
%             -5,-75.5555555555556,4.44444444444445,128.888888888889];
% QQW{1} = QelpTest;



% figure(2)
% %plotObs(Qsph1, [-0.3 -0.3 -0.3], [0.3, 0.3, 0.3]);
% % 2
% % plotObs(Qsph1_W, [-0.3 1.9-0.3 -0.3], [0.3, 1.9+0.3, 0.3]);
% % hold on
% % plotObs(Qpl1_W, [-0.5 rmin-0.2 -1], [0.5, rmax, 0.5]);
%
% LB{2} = [-0.5 rmin-0.2 -1]; LB{1} = [-0.2 1.5 -0.3];
% UB{2} = [0.5 rmax 0.5]; UB{1} = [.2 2 .3];
% plotObs(QQW, LB, UB);
% xlabel('X [m]')
% ylabel('Y [m]')
% zlabel('Z [m]')
% axis equal

% figure(2)
% plotObs(Qcon1, [-0.1, -0.1, -0.1], [0.1, 0.1, 0.1]);
