%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [Inr, Inr_sq, Inrdp] = computeIv2(sim)
global BN;
Inr = zeros(BN,1);
Inr_sq = zeros(BN,1);
Inrdp = zeros(BN,1);
if ~isempty(sim.dPhi)
sim.nr = abs(sum(sim.NN.*sim.RR,2)); %I = dot(n,r);
sim.Inr = sim.nr;
sim.nr_sq = sim.nr.^2;
sim.Inr_sq = sim.nr_sq;
sim.Inrdp = sim.Inr.*sim.dPhi/max(sim.dPhi);
sim.usbin = unique(sort(sim.bin)); %unique sorted bin

sim.usInr = zeros(size(sim.usbin));
sim.usInr_sq = zeros(size(sim.usbin));
sim.usInrdp = zeros(size(sim.usbin));
for cnt = 1:length(sim.bin)
    bin = sim.bin(cnt);
    ind = find(sim.usbin == bin);
    sim.usInr(ind) = sim.usInr(ind) + sim.Inr(cnt);
    sim.usInr_sq(ind) = sim.usInr_sq(ind) + sim.Inr_sq(cnt);
    sim.usInrdp(ind) = sim.usInrdp(ind) + sim.Inrdp(cnt);
end
Inr(sim.usbin) = sim.usInr; 
Inr_sq(sim.usbin) = sim.usInr_sq; 
Inrdp(sim.usbin) = sim.usInrdp;
end