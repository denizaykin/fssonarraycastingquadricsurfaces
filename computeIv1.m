%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [Inr, Inr_sq, Inrdp] = computeIv1(sim, rmin, rmax)
global NUM BN tpl c
if ~isempty(sim.dPhi)
MM = BN*NUM;

sim.nr = abs(sum(sim.NN.*sim.RR,2)); %I = dot(n,r);
sim.Inr = sim.nr;
sim.nr_sq = sim.nr.^2;
sim.Inr_sq = sim.nr_sq;
sim.Inrdp = sim.Inr.*sim.dPhi/max(sim.dPhi);
sim.usbin = unique(sort(sim.bin)); %unique sorted bin

% sim.usInr = zeros(size(sim.usbin));
% sim.usInr_sq = zeros(size(sim.usbin));
% sim.usInrdp = zeros(size(sim.usbin));
% for cnt = 1:length(sim.bin)
%     bin = sim.bin(cnt);
%     ind = find(sim.usbin == bin);
%     sim.usInr(ind) = sim.usInr(ind) + sim.Inr(cnt);
%     sim.usInr_sq(ind) = sim.usInr_sq(ind) + sim.Inr_sq(cnt);
%     sim.usInrdp(ind) = sim.usInrdp(ind) + sim.Inrdp(cnt);
% end



tmin = 2*rmin/c;
tmax = 2*rmax/c;
t = linspace(tmin,tmax,MM);
%dt = (tmax-tmin)/(MM-1);



%gamma = 0;
%Pres = zeros(1,MM);
Ires = zeros(1,MM); Inr_ = Ires; Inrsq_ = Ires;
%N = getNoPtsatRange(sim.R, Ranges);
for i = 1:length(sim.bin)
    ti = 2*sim.R(i)/c;
%     [valR, indR] = find(Ranges < sim.R(i));
%     [No] = find(Ranges(indR(end)) <= Ri & Ri < Ranges(indR(end)+1));
%     N = length(No);
%N = 1;
    %f_ = cos(2*pi*freq*(t-ti)-1.68)/N.*[mystep(t-(ti-gamma*tpl)) - mystep(t-(ti+(1-gamma)*tpl))];
    %f_ = cos(2*pi*freq*(t-ti))/N.*[mystep(t-(ti-gamma*tpl)) - mystep(t-(ti+(1-gamma)*tpl))];
    %f_ = 1/N*(cos(2*pi*freq*(t-ti))).^2.*[mystep(t-ti) - mystep(t-(ti+tpl))];
    %dphi = getdphiv2(Ri(i), yc, rc, dR);
    %f_ = cos(2*pi*freq*(t+ti)-gamma)/N(i).*[mystep(t-ti) - mystep(t-(ti+tpl))];
%    Pres = Pres + sqrt(sim.Inrdp(i)) * f_;
    Ires = Ires + sim.nr(i) * sim.dPhi(i)*[mystep(t-ti) - mystep(t-(ti+tpl))];
    Inr_ = Inr_ + sim.nr(i)*[mystep(t-ti) - mystep(t-(ti+tpl))];
    Inrsq_ = Inrsq_ + sim.nr_sq(i)*[mystep(t-ti) - mystep(t-(ti+tpl))];
end
%Pres = reshape(Pres,NUM,512);
%IpowNRdphi = real((1/NUM*sum(Pres.^2)));
%nIpowNRdphi = IpowNRdphi/max(IpowNRdphi);

% nIpowNRdphi_br = normI(Ires);
% nIpowNR_br = normI(Inr_); 
% nIpowNRsq_br = normI(Inrsq_);

Inr = reshapeI(Inr_); 
Inr_sq = reshapeI(Inrsq_); 
Inrdp = reshapeI(Ires);
else
Inr = zeros(BN,1);
Inr_sq = zeros(BN,1);
Inrdp = zeros(BN,1);
end