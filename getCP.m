%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function X = getCP(coef, R, T)
%coef: coefficients defining a quartic
%R: range
%T: theta
%X: critical point on quartic defined by coef
eps = 1e-7;
if size(R,2) > 1
    R = R';
end
if length(T) == 1
    T = T*ones(size(R));
end
A = coef(1); B = coef(2); C = coef(3); D = coef(4); E = coef(5);
F = coef(6); G = coef(7); H = coef(8); I = coef(9); J = coef(10);

tanT = tan(T);
tanTsq = tanT.^2;
K = A*tanTsq + B - C*(1 + tanTsq) + D*tanT;
L = G*tanT + H;
M = C*R.^2 + J;
N = E*tanT + F;

a = K.^2 + N.^2.*(1+tanTsq);
b = 2*K.*L + 2*N.*I.*(1 + tanTsq);
c = 2*K.*M + L.^2 - (R.*N).^2 + I.^2.*(1+tanTsq);
d = 2*L.*M - 2*N.*I.*R.^2;
e = M.^2 - (I.*R).^2;

A_coef = [a b c d e];
y = QuarticMA(A_coef);
% y = zeros(size(A,1),4);
% for i = 1:size(A_coef,1)
%     y(i,:) = roots(A_coef(i,:)); 
% end
RR = repmat(R',4,1); RR = RR(:);
TT = repmat(T',4,1); TT = TT(:);
y = y'; y = y(:);
indsImag = find(abs(imag(y)) >= eps);
y(indsImag) = [];
if isempty(y)
    X = [];
    return
end
RR(indsImag) = [];
TT(indsImag) = [];
y = real(y);
tanTT = tan(TT);
x = y.*tanTT;
zpos = sqrt(RR.^2 - y.^2.*(1+tanTT.^2));

indsBad = find(imag(zpos) ~= 0);
x(indsBad) = [];
if isempty(x)
    X = [];
    return
end   
y(indsBad) = [];
zpos(indsBad) = [];

zneg = -zpos;
x = [x; x];
y = [y; y];
z = [zneg; zpos];

[z, inds] = sort(z);
x = x(inds);
y = y(inds);

Ti = atan(x./y); 
indsBad = find(Ti > max(T) + eps | Ti < min(T) - eps); %eliminate if theta's are not right.
x(indsBad) = [];
if isempty(x)
    X = [];
    return
end  
y(indsBad) = [];
z(indsBad) = [];

X = [x y z];
X = elimUnfit(coef, X); %eliminate the points that are not on the quartic.