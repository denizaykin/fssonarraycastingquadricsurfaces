%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
close all; clear
% Define small values to account for inaccurate precision
global eps eps2
eps = 0.001;
eps2 = 1e-4;

%Define the quadratic
Q =  sqrt(10)*[1    -2     1; 1     2     1; 1     5     6];
%Q =  sqrt(10)*[1    0     1; 1     2     1; 1     5     6];
%Q =  sqrt(10)*[1    0     1; 1     2     0; 1     0     6];
%Q =  sqrt(10)*[1    0     1; 1     2     0; 1     0     1];
%Q =  sqrt(900)*eye(3);

K0 = Q'*Q;
K0 = getQelps([0.1 0.2 0.3]',0); 
%K = [1 0 0; 0 1 0; 0 0 1];
%figure(1)
% ellipsoid(K0);
% hold on
coeffs0 = mat2coeffs(K0);
%T1 = [0.1 -1.5 0.1]';
%w1 = [0.05 0.1 0.1]';
% T1 = [0.1 -1.7 -0.3]';
% w1 = [0.2 pi/2 0.2]';
T1 = [0 -1.7 0]';
w1 = [0 pi/3 0]';
R1 = rodrigues(w1);

%coeffs1_ = updateCoeffs(coeffs0, R1, T1)
[coeffs1, Q_gen1] = updateCoeffsGen(coeffs0, R1, T1);
%K1 = coeffs2mat(coeffs1);
%ellipsoid(K1, -R1*T1);

rmin = 1.25; windowlength = 1.25; rmax = rmin + windowlength; dR = windowlength/511;
thetaMax = 14*pi/180; phiMax = 7*pi/180;
bins_Crit = 1:512; R_Crit = rmin + (bins_Crit - 1)*dR; %theta = 0;
theta_Crit = pi/180*linspace(-14,14,96);
%theta_Crit = 0;
[RR, TT] = meshgrid(R_Crit, theta_Crit);

Limits.rmin = rmin;
Limits.rmax = rmax;
Limits.phiMax = phiMax;
Limits.thetaMax = thetaMax;

CP = getCP2(coeffs1, RR, TT, Limits);


%plotQ(coeffs1,rmin,rmax,thetaMax,phiMax)
%for theta = pi/180*linspace(-14,14,96)


if ~isempty(CP)
    CP = elimShPts(CP,Q_gen1);
end
if ~isempty(CP)
    CP = elimUnfit(coeffs1, CP); %eliminate the points that are not on the quartic.
end
if ~isempty(CP)
    
    limitsOb1 = [min(CP(:,1))-dR, max(CP(:,1)) + dR, min(CP(:,2))-dR, max(CP(:,2)) + dR, min(CP(:,3))-dR, max(CP(:,3)) + dR];
    lb1 = limitsOb1([1, 3, 5]); ub1 = limitsOb1([2 4 6]);
    
    figure(1)
    plotOb(coeffs1, lb1, ub1);
    hold on
    plot3(CP(:,1),CP(:,2),CP(:,3),'.')
    xlabel('X [m]')
    ylabel('Y [m]')
    zlabel('Z [m]')
    axis equal
    
    figure(2)
    plotOb(coeffs1, lb1, ub1);
    hold on
    axis_2 = plotLimits(Limits);
    axis equal
    plot3(CP(:,1),CP(:,2),CP(:,3),'.k', 'Linewidth',1)
    xlabel('X [m]', 'fontSize', 16)
    ylabel('Y [m]', 'fontSize', 16)
    zlabel('Z [m]', 'fontSize', 16)
    axis_3 = axis_2; axis_3(3) = 0;
    axis(axis_3);
    plotDidson;
    view([0.2,-0.3,0.2]);
    
    figure(3)
    hold on
    plot3(CP(:,1),CP(:,2),CP(:,3),'.')
    xlabel('X [m]')
    ylabel('Y [m]')
    zlabel('Z [m]')
else
    warning('No Critical points are found. Object is probably outside the field of view.')
end
%end




