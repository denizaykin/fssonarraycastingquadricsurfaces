%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
close all; clear
InitParams
InitObjects %Initialize the objects
InitSonarMotions %Initialize the sonar motions

CP = getCP2(Qelp1_W, RR, TT, Limits);


if ~isempty(CP)
    figure(1)
    [LB, UB] = getObLimits(CP);
    plotObs(QQW, LB, UB);
    plotLimitsLite(Limits, Rots, Trans);
    plotDidsons(Rots, Trans);
    %    showResults
end

    CP = refineCP(CP, Qelp1_W);
if ~isempty(CP)
    plotCPs(CP);
    lighting phong
    xlabel('X [m]', 'fontSize', 16)
    ylabel('Y [m]', 'fontSize', 16)
    zlabel('Z [m]', 'fontSize', 16)
    axis equal
    view([0.2,-0.3,0.2]);
else
    warning('No Critical points are found. Object is probably outside the field of view.')
end


