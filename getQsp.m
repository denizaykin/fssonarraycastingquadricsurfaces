%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function Qspm = getQsp(varargin)
if length(varargin) == 1
    rad = varargin{1};
else
    rad = 1;
end
Qsp = [eye(3) [0 0 0]'; 0 0 0 -1];
M = getMag(rad*ones(3,1));
Qspm = inv(M)*Qsp*inv(M)';