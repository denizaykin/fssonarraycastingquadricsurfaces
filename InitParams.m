%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
% Define small values to account for inaccurate precision
global eps eps2 eps3 epsUnfit epsimag dxDid dyDid dzDid col colno NUM BN BeN tpl c testcnt
epsimag = 1e-5;
epsUnfit = 1e-2;
eps = 1e-4;
eps2 = 1e-4;
eps3 = 1e-4;
dxDid = 0.03;
dyDid = 0.08;
dzDid = 0.015;
col = 'krbcmy';
colno = 1;
NUM = 100;
BN = 512;
BeN = 96;
tpl = 4.4e-6;
c = 1500; % [m/s] : speed of sound in water
testcnt = 1;

%Initialize sonar related parameters
rmin = 1.25; windowlength = 1.25; rmax = rmin + windowlength; dR = windowlength/511;
thetaMax = 14.4*pi/180; phiMax = 7*pi/180;
bins_Crit = 1:BN; R_Crit = rmin + (bins_Crit - 1)*dR; %theta = 0;
theta_Crit = pi/180*linspace(-14,14,BeN);
%theta_Crit = 0;
[RR, TT] = meshgrid(R_Crit, theta_Crit);
%[TT, RR] = meshgrid(theta_Crit, R_Crit);

%Limits to be scanned
Limits.rmin = rmin;
Limits.rmax = rmax;
Limits.phiMax = phiMax;
Limits.thetaMax = thetaMax;

%Define the sonar motion (i.e. rotation and translation along the
%trajectory)
Rots{1} = eye(3);
Trans{1} = zeros(3,1);

%w2 = [0 -1.4 0]; %causing the problem at the 15th beam with second
%ellipse.
w2 = [0 pi/2 0];
Rots{2} = rodrigues(w2);
Trans{2} = [0.1 0 0]';

w3 = [-0.1 0 0.2];
Rots{3} = rodrigues(w3);
Trans{3} = [0.1 0 -0.2]';