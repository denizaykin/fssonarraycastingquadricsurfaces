%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
% Ms{1} =  eye(4,1);
% Ms{2} = [eye(3) [0.1 0 0]'; 0 0 0 1];
for i = 1:length(Rots)
    Ms{i} = [Rots{i} Trans{i}; 0 0 0 1];
end


if ~iscell(QQW) %if there is only one object
    for j = 1:length(Ms)
        QAll{1,j} = tr2W(QQW, Ms{j}); %Note that QQ contains the objects wrt world coordinates
        if ~isempty(LIMW)
            if ~isempty(LIMW.LB{1})
                LIMAll.LB{1,j} = tr2W(LIMW.LB{1}, Ms{j});
            else
                LIMAll.LB{1,j} = [];
            end
            if ~isempty(LIMW.UB{1})
                LIMAll.UB{1,j} = tr2W(LIMW.UB{1}, Ms{j});
            else
                LIMAll.UB{1,j} = [];
            end
        end
    end
else
    for i = 1:length(QQW) %for all objects
        for j = 1:length(Ms) %for all sonar views
            QAll{i,j} = tr2W(QQW{i}, Ms{j}); %Note that QQ contains the objects wrt world coordinates
            if ~isempty(LIMW)
                if ~isempty(LIMW.LB{i})
                    if iscell(LIMW.LB{i})
                        for k = 1:length(LIMW.LB{i})
                            LIMAll.LB{i,j}{k} = tr2W(LIMW.LB{i}{k}, Ms{j});
                        end
                    else
                        LIMAll.LB{i,j}{1} = tr2W(LIMW.LB{i}, Ms{j});
                    end
                else
                    LIMAll.LB{i,j} = [];
                end
                if ~isempty(LIMW.UB{i})
                    if iscell(LIMW.UB{i})
                        for k = 1:length(LIMW.LB{i})
                            LIMAll.UB{i,j}{k} = tr2W(LIMW.UB{i}{k}, Ms{j});
                        end
                    else
                        LIMAll.UB{i,j}{1} = tr2W(LIMW.UB{i}, Ms{j});
                    end
                else
                    LIMAll.UB{i,j} = [];
                end
            else
                LIMAll = [];
            end
        end
    end
end