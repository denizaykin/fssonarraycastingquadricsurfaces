%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [CP, CPplot] = elimShPts(CP, Q)
global epsimag epst_R
R = sqrt(CP(:,1).^2 + CP(:,2).^2 + CP(:,3).^2);
u = [CP./repmat(R,1,3) zeros(size(CP,1),1)];
p = [zeros(size(CP)), ones(size(CP,1),1)];
pQ = p*Q';
uQ = u*Q';
uQp = -sum(u.*pQ,2); %N by 4
uQu = sum(u.*uQ,2);
pQp = sum(p.*pQ,2);
delta = sqrt(uQp.^2 - uQu.*pQp);
%t1 = (uQp + delta)./uQu;
t2 = (uQp - delta)./uQu;
indsNoCross = abs(imag(t2)) > epsimag;
t2(indsNoCross) = R(indsNoCross);
t2 = real(t2);

%Note that we need R <= t2 then t2 - R >= 0 or t2 - R >= -eps2
%So we need to eliminate t2-R < -eps2
indsBad = real(t2-R) < -epst_R;
CPplot = repmat(t2./R,1,3).*CP;
CPplot(indsBad,:) = [];
CP(indsBad,:) = [];
