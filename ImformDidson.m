%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [Inr, Inr_sq, Inrdp] = ImformDidson(CP_rtp, index, QQ, rmin, rmax, thetaMax, phiMax)
global BN BeN testcnt caseno
%dd = 0.1;
if ~iscell(CP_rtp)
    dR = (rmax - rmin)/511;
    dT = 2*thetaMax/95;
    Inr = zeros(BN, BeN);
    Inr_sq = zeros(BN, BeN);
    Inrdp = zeros(BN, BeN);
    [f, cal_a]= GetDistortionCoefs(caseno);
    for i = 1:length(index)-1
        CP_rtp_line = CP_rtp(index(i):index(i+1)-1,:);
        R = CP_rtp_line(:,1); bin = ceil(1 + (R-rmin)*511/(rmax-rmin)); %bin = round((R-rmin)/dR);
        T = CP_rtp_line(:,2); Tdeg = T*180/pi; beam = [Tdeg.^3 Tdeg.^2 Tdeg ones(size(Tdeg))]*[cal_a']; beam = ceil(beam); %beam = ceil(T/dT)+48;
        Phi = CP_rtp_line(:,3);
        x = R.*sin(T).*cos(Phi);
        y = R.*cos(T).*cos(Phi);
        z = R.*sin(Phi);
        
        CP = [x y z];
        [Rmax, inds] = max(R);
        zmax = z(inds);
        phiMax = asin(zmax./Rmax);
        phiMid = mean(phiMax);
        w = [phiMid, 0 0];
        Rot = rodrigues(w);
        zr = CP*Rot(:,3);
        [val, indz] = sort(zr);
        x = x(indz); y = y(indz); z = z(indz); %sorted values as patches.
        bin = bin(indz); beam = beam(indz); R = R(indz); T = T(indz);
        
        
        %Group into patch regions by eliminating the disconnected patches
        %that are separated by more than dR: i.e. object occludance


        NN = -getNormal([x,y,z],QQ);   sim.NN = -NN(1:end-1,:);
        RR = [x, y, z]./repmat(R,1,3); sim.RR = RR(1:end-1,:);
        Phis = asin(z./R);%acos((y(1:end-1).*y(2:end) + z(1:end-1).*z(2:end))./(R(1:end-1).*R(2:end))); sim.dPhi = dPhi;
        dPhi = Phis(2:end) - Phis(1:end-1); sim.dPhi = dPhi;
        
        R1 = R(1:end-1); R2 = R(2:end);
        indsR1 = find(R1 <= R2); indsR2 = find(R2 < R1);
        sim.R = zeros(length(R1),1);
        sim.R(indsR1) = R1(indsR1);
        sim.R(indsR2) = R2(indsR2);
        sim.bin = ceil(1 + (sim.R-rmin)*511/(rmax-rmin));
        sim.X = x(1:end-1);
        sim.Y = y(1:end-1);
        sim.Z = z(1:end-1);
        
        % testcnt = testcnt+1
        % if testcnt == 81
        % stop;
        % end
        [Inr(:,beam(1)), Inr_sq(:,beam(1)), Inrdp(:,beam(1))] = computeIv1(sim, rmin, rmax);
        
        %Inr = abs(sum(NN.*RR,2)); %sim.Inr = Inr;
        %Inrdp = sim.Inr.*dPhi; %sim.Inrdphi = Inrdp
        
        %                 figure(2)
        %                 hold on
        %                 plot3(x,y,z); plot3(x,y,z, '.')
        %                 axis equal
        %                 %text(x+0.001,y,z, num2str(bin))
        %                 for i = 1:length(x)
        %                     plot3([x(i), x(i) + 0.01* NN(i,1)],[y(i), y(i) + 0.02* NN(i,2)],[z(i), z(i) + 0.02* NN(i,3)])
        %                 end
        %axis([min(x)-dd, max(x)+dd, 0 , max(y)+dd, min(z)-dd, max(z)+dd])
        %         CP = [x y z];
        %         [Rmax, inds] = max(R);
        %         zmax = z(inds);
        %         phiMax = asin(zmax./Rmax);
        %         phiMid = mean(phiMax);
        %         w = [phiMid, 0 0];
        %         Rot = rodrigues(w);
        %         CProt = CP*Rot;
        %         xr = CProt(:,1); yr = CProt(:,2); zr = CProt(:,3);
        %         figure(2)
        %         hold on
        %         plot3(xr,yr,zr); plot3(xr,yr,zr, '.')
        %         axis equal
        %         text(x+0.001,y,z, num2str(bin))
        %         [val, indz] = sort(zr);
        %         x = x(indz); y = y(indz); z = z(indz);
        %         figure(3)
        %         hold on
        %         plot3(x,y,z); plot3(x,y,z, '.')
        %         axis equal
        %         text(x+0.001,y,z, num2str(bin))
        
        %axis([min(x)-dd, max(x)+dd, 0 , max(y)+dd, min(z)-dd, max(z)+dd])
        %         elimShPts([x(3), y(3), z(3)], QQ)
    end
else
    for i = 1:size(CP_rtp,1) %for all objects
        for j = 1:size(CP_rtp,2) % for all motions
            if i == 1
                [Inr{j}, Inr_sq{j}, Inrdp{j}] = ImformDidson(CP_rtp{i,j}, index{i,j}, QQ{i,j},rmin,rmax,thetaMax,phiMax);
            else
                [Inr_, Inr_sq_, Inrdp_] = ImformDidson(CP_rtp{i,j}, index{i,j}, QQ{i,j},rmin,rmax,thetaMax,phiMax);
                Inr{j} = Inr{j} + Inr_;
                Inr_sq{j} = Inr_sq{j} + Inr_sq_;
                Inrdp{j} = Inrdp{j} + Inrdp_;
            end
        end
    end
end