%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function roots = quartic_(A)
if length(A) ~= 5
    error('Quartic expected but length(A) is not equal to 5');
end
a = A(1);
if abs(a) <= eps 
    display('Not a quartic')
    roots = cubic_(A(2:end));
    return
end
b = A(2);
c = A(3);
d = A(4);
e = A(5);

Delta0 = c^2 - 3*b*d + 12*a*e;
Delta1 = 2*c^3 - 9*b*c*d + 27*b^2*e + 27*a*d^2 - 72*a*c*e;
Q = power((Delta1 + sqrt(Delta1^2 - 4*Delta0^3))/2, 1/3);
p = (8*a*c - 3*b^2)/(8*a^2);
q = (b^3 - 4*a*b*c + 8*a^2*d)/(8*a^3);
S = 1/2*sqrt(-2/3*p + 1/3/a*(Q + Delta0/Q));


x1 = -b/4/a - S + 1/2*sqrt(-4*S^2 - 2*p + q/S);
x2 = -b/4/a - S - 1/2*sqrt(-4*S^2 - 2*p + q/S);
x3 = -b/4/a + S + 1/2*sqrt(-4*S^2 - 2*p - q/S);
x4 = -b/4/a + S - 1/2*sqrt(-4*S^2 - 2*p - q/S);

roots = [x1 x2 x3 x4]';
