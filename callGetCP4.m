%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function X = callGetCP4(QAll, LIMAll, R, T, Limits)
emptyFlag = 1;
for i = 1:size(QAll,1) %For objects
    for j = 1:size(QAll,2) %For all sonar motions
        X{i,j} = getCP4(QAll{i,j}, LIMAll.LB{i,j}, LIMAll.UB{i,j}, R, T, Limits);
        if emptyFlag & ~isempty(X{i,j})
            emptyFlag = 0;
        end
    end
end
if emptyFlag 
    X = [];
end