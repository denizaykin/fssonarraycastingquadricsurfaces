%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function K = coeffs2mat(coeffs)
K = diag(coeffs(1:3));
K(1,2) = coeffs(4)/2; K(2,1) = K(1,2);
K(1,3) = coeffs(5)/2; K(3,1) = K(1,3);
K(2,3) = coeffs(6)/2; K(3,2) = K(2,3);