%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function fill3Rec(varargin)
if length(varargin) == 1
    face = varargin{1};
    C = [0.5, 0.5, 0.5];
elseif length(varargin) == 2
    face = varargin{1};
    C = varargin{2};
elseif length(varargin) == 4
    face = [varargin{1}, varargin{2}, varargin{3}, varargin{4}];
    C = [0.5, 0.5, 0.5];
else
    face = [varargin{1}, varargin{2}, varargin{3}, varargin{4}];
    C = varargin{5};
end
fill3(face(1,:), face(2,:), face(3,:), C, 'edgecolor', 'black')