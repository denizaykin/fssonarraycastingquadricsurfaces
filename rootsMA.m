%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function rs = rootsMA(A)
rs = NaN(size(A,1), size(A,2)-1);
for i = 1:size(A,1)
     r = roots(A(i,:));
     rs(i,end-length(r)+1:end) = r;
end