%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function Ipow = reshapeI(I)
global NUM BN
I= reshape(I,NUM,BN);
Ipow = real(1/NUM*sum(I));