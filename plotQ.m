%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function plotQ(c,rmin,rmax,thetaMax,phiMax)
dx = 0.01;
gx = -rmax*sin(thetaMax):dx:rmax*sin(thetaMax);
gy = rmin*cos(thetaMax):dx:rmax;
gz = -rmax*sin(phiMax):dx:rmax*sin(phiMax);
%gv = linspace(-1,2,100); % adjust for appropriate domain
[xx, yy, zz]=meshgrid(gx, gy, gz);
F = c(1)*xx.*xx + c(2)*yy.*yy + c(3)*zz.*zz+ c(4)*xx.*yy + c(5)*xx.*zz + c(6)*yy.*zz + c(7)*xx + c(8)*yy + c(9)*zz + c(10);

figure(1)
isosurface(xx, yy, zz, F, 0)