%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function M = RT2M(R,T)
M = [R T; 0 0 0 1];