%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function index = groupT(Ti)
global eps2
tcurr = Ti(1);
index = 1;
for i = 2:length(Ti)
    tnext = Ti(i);
    if abs(tcurr - tnext) > eps2
       tcurr = tnext;
       index = [index; i];
    end    
end
index = [index; length(Ti)+1];