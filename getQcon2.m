%
%  @Copyright (c) 2021 by Murat Aykin.  All Rights Reserved.
%
function [Obcon, ObLim] = getQcon2(varargin)
if isempty(varargin)
    rad = 1;
    h = 1;
    hcut = h;
    solid = 1;
elseif length(varargin) == 1
    rad = varargin{1};
    h = 1;
    hcut = h;
    solid = 1;
elseif length(varargin) == 2
    rad = varargin{1};
    h = varargin{2};
    hcut = h;
    solid = 1;
    rad = rad/h;
elseif length(varargin) == 3
    rad = varargin{1};
    h = varargin{2};
    hcut = varargin{3};
    solid = 1;
    rad = rad/h;
else
    rad = varargin{1};
    h = varargin{2};
    hcut = varargin{3};
    solid = varargin{4};
    rad = rad/h;
end
Qcon = [eye(3) [0 0 0]'; 0 0 0 0]; Qcon(3,3) = -1;
M = getMag([rad, rad, 1]);
Qcon = inv(M)*Qcon*inv(M)';

Tcon = [0 0 -h]';
wcon = [0 0 0]';
Rcon = rodrigues(wcon);
Qcon = tr2W(Qcon, Rcon, Tcon);

%Get boundaries
Qlb = getQplv2();
Qub = Qlb;
Tub = [0 0 -hcut]';
%wub = [0 0 0]';
Rub = eye(3);
Qub = tr2W(Qub, Rub, Tub);

Obcon{1} = Qcon;
ObLim.LB{1} = Qlb; %Lower limit for Qcon
ObLim.UB{1} = Qub; %High limit for Qcon

if solid
    Obcon{2} = Qlb;
    Obcon{3} = Qub;
    ObLim.LB{2} = [];
    ObLim.LB{3} = [];
    ObLim.UB{2} = Qcon; %High limit for Qlb
    ObLim.UB{3} = Qcon; %High limit for Qub
end